organization := "com.tsaplin"

name := "leet-code"

version := "0.1.0"

scalaVersion := "2.12.8"

libraryDependencies ++= Seq(
  "com.typesafe"                  % "config"                        % "1.3.3",
  "com.typesafe.scala-logging"    %% "scala-logging"                % "3.9.2",
  "ch.qos.logback"                % "logback-classic"               % "1.1.3",
  "org.scalatest"                 % "scalatest_2.12"                % "3.0.1"                 % "test"
)
