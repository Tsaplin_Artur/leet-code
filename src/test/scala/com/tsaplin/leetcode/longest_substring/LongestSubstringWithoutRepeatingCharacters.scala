package com.tsaplin.leetcode.longest_substring

import org.scalatest.FlatSpec

import scala.collection.mutable.ListBuffer

class LongestSubstringWithoutRepeatingCharacters extends FlatSpec {

  object Solution {
    def find(str: String): (Int, String) = {
      val resultListOfLists = new ListBuffer[List[Char]]()
      val resAccumulator: List[Char] = str.toCharArray.foldLeft(List[Char]()) { (accumulator, current) =>
        if (accumulator.isEmpty) accumulator :+ current
        else if (accumulator.nonEmpty && accumulator.contains(current) && accumulator.head.equals(current)) {
          resultListOfLists += List(accumulator.head)
          accumulator.slice(1, accumulator.length) :+ current
        }
        else if (accumulator.nonEmpty && accumulator.contains(current) && !accumulator.head.equals(current)) {
          resultListOfLists += accumulator
          List(current)
        }
        else if (accumulator.nonEmpty && !accumulator.contains(current)) {
          accumulator :+ current
        }
        else accumulator
      }
      resultListOfLists += resAccumulator
      val findingResult = resultListOfLists.maxBy(_.length)
      (findingResult.length, findingResult.mkString)
    }
    def lengthOfLongestSubstringViaFold(str: String): Int = {
      val lengthStringPair = find(str)
      val reverseLengthStringPair = find(str.reverse)
      Seq(lengthStringPair, reverseLengthStringPair).maxBy(_._1)._1
    }
    def lengthOfLongestSubstring(s: String): Int = {
      s.scanLeft("")((currStr: String, currChar: Char) => currStr.substring(1 + currStr.indexOf(currChar)) + currChar)
        // Up to this point we would get vector like this
        // Vector(, a, ab, abc, bca, cab, abc, cb, b)
        // now if we take max of length would get the answer
        .map(_.length)
        .reduce(Math.max)
    }
  }

  "sandbox" should "be here" in {
    val str = ""
    println(str.indexOf("c"))
  }

  "test 0" should "be successful" in {
    val str = "abcabcbb"
    val resultLength = Solution.lengthOfLongestSubstring(str)
    assert(resultLength.equals(3))
  }

  "test 1" should "be successful" in {
    val str = "bbbbb"
    val resultLength = Solution.lengthOfLongestSubstring(str)
    assert(resultLength.equals(1))
  }

  "test 2" should "be successful" in {
    val str = "pwwkew"
    val resultLength = Solution.lengthOfLongestSubstring(str)
    assert(resultLength.equals(3))
  }

  "test 3" should "be successful" in {
    val str = "dvdf"
    val resultLength = Solution.lengthOfLongestSubstring(str)
    assert(resultLength.equals(3))
  }

  "test 4" should "be successful" in {
    val str = "asjrgapa"
    val resultLength = Solution.lengthOfLongestSubstring(str)
    assert(resultLength.equals(6))
  }

  "test 5" should "be successful" in {
    val str = "bpoiexpqhmebhhu"
    val resultLength = Solution.lengthOfLongestSubstring(str)
    assert(resultLength.equals(8))
  }

}

