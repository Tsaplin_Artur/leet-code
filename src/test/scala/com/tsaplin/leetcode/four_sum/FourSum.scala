package com.tsaplin.leetcode.four_sum

import org.scalatest.FlatSpec

import scala.collection.mutable

class FourSum extends FlatSpec {

  // https://leetcode.com/problems/4sum/
  //
  // Given an array nums of n integers and an integer target, are there elements a, b, c, and d in nums such that a + b + c + d = target? Find all unique quadruplets in the array which gives the sum of target.
  //
  // Note:
  //
  // The solution set must not contain duplicate quadruplets.
  //
  // Example:
  //
  // Given array nums = [1, 0, -1, 0, -2, 2], and target = 0.
  //
  // A solution set is:
  // [
  //  [-1,  0, 0, 1],
  //  [-2, -1, 1, 2],
  //  [-2,  0, 0, 2]
  // ]

  object Solution {

    def fourSum(nums: Array[Int], target: Int): List[List[Int]] = {
      val result = new mutable.HashSet[List[Int]]()
      val balanceStorage = new mutable.HashMap[Integer, List[Int]]
      if (nums == null || nums.length < 4) result.toList else {
        for (i <- nums.indices) {
          for (j <- i + 1 until nums.length) {
            for (k <- j + 1 until nums.length) {
              val balance = target - nums(i) - nums(j) - nums(k)
              if (balanceStorage.contains(balance) && balanceStorage(balance).head == i && balanceStorage(balance)(1) == j){
                val tmpRes = List(nums(i), nums(j), nums(k), balance)
                val sortedTempRes = tmpRes.sortWith(_ < _)
                result.+=(sortedTempRes)
              }
              balanceStorage.put(nums(k), List(i, j))
            }
          }
        }
        result.toList
      }
    }

  }

  "test 0" should "be successful" in {
    val testArray = Array(1, 0, -1, 0, -2, 2)
    val target = 0
    val expectedResult = List(List(-2, 0, 0, 2), List(-1, 0, 0, 1), List(-2, -1, 1, 2))
    val expectedResultString = expectedResult.mkString(", ")
    val result = Solution.fourSum(testArray, target)
    val resultString = result.mkString(", ")
    println("expected result: " + expectedResultString)
    println("result: " + resultString)
    assert (result == expectedResult)
  }

}
