package com.tsaplin.leetcode.max_points_in_sequence

import org.scalatest.FlatSpec

import scala.collection.immutable

class MaxPointsTinkoff extends FlatSpec {

  /**
   * Есть список из интов. Вывести значения локальных максимумов
   * Для List(1, 2, 5, 4, 6, 2) ответ List(5, 6)
   * Для List(1, 2, 2, 4) ответ List() - максимумов нет
   * Для List(1, 3, 3, 1) ответ List(3) - плато из троек также является локальным максимумом
   */

  object MaxPoints {

    def sortDuplications(numbers: Seq[Int]): immutable.Seq[Int] = {
      numbers.foldLeft(List[Int]()) {
        case (list, elem) => if (list.lastOption.contains(elem)) { list } else list :+ elem
      }
    }

    def localMax(numbers: Seq[Int]): Seq[Int] = {
      val indexedNumbers: Seq[(Int, Int)] = sortDuplications(numbers).zipWithIndex
      val indexedNumbersMap = indexedNumbers.map(elem => elem._2 -> elem._1).toMap
      indexedNumbers.filter(elem => { // здесь сортируем точки экстремум
        val prevEl: Option[Int] = indexedNumbersMap.get(elem._2 - 1)
        val nextEl: Option[Int] = indexedNumbersMap.get(elem._2 + 1)
        var result: Boolean = false
        if (prevEl.isEmpty && nextEl.isEmpty) {
          result = true
        }
        if (prevEl.isEmpty && nextEl.isDefined) {
          if (elem._1 > nextEl.get) {
            result = true
          }
        }
        if (prevEl.isDefined && nextEl.isEmpty) {
          if (elem._1 > prevEl.get) {
            result = false
          }
        }
        if (prevEl.isDefined && nextEl.isDefined) {
          if (elem._1 > prevEl.get && elem._1 > nextEl.get) {
            result = true
          }
        }
        result
      }).map(elem => elem._1)
    }

  }

  "test 0" should "be successful" in {

    println(MaxPoints.localMax(List(1, 2, 5, 4, 6, 2)))

    assert(MaxPoints.localMax(List(1, 2, 5, 4, 6, 2)).equals(List(5, 6)))

    println(MaxPoints.localMax(List(1, 2, 2, 4)))

    assert(MaxPoints.localMax(List(1, 2, 2, 4)).equals(List()))

    println(MaxPoints.localMax(List(1, 3, 3, 1)))

    assert(MaxPoints.localMax(List(1, 3, 3, 1)).equals(List(3)))

    println(MaxPoints.localMax(List(1, 3, 1)))

    assert(MaxPoints.localMax(List(1, 3, 3, 1)).equals(List(3)))

  }

}
