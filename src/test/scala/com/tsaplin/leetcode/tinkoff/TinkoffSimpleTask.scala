package com.tsaplin.leetcode.tinkoff

import org.scalatest.FlatSpec

import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}
import scala.util.{Failure, Success}

class TinkoffSimpleTask extends FlatSpec {

  implicit val executionContext: ExecutionContextExecutor = ExecutionContext.global

  // Есть список из интов. Подсчитать количество перемен знака в последовательности (ноль не перемена)
  // val in = List(1,-2,3,3,4,-2,-2,-3,1)
  def foolInterviewSolution(listOfIntegers: List[Int]): Int = {
    var counter = 0
    listOfIntegers.reduce((a, b) => {
      if ((a * b) < 0) counter+=1
      a
    })
    counter
  }

  def wellSolution(listOfIntegers: List[Int]): Int = {
    listOfIntegers.map(elem => (0, elem)).reduce((a, b) => if (a._2 * b._2 < 0) (a._1 + 1, a._2) else (a._1, a._2))._1
  }

  def foolRecursionSolution(listOfIntegers: List[Int], enterPoint: Int, count: Int): Int = {
    if (enterPoint == (listOfIntegers.size - 1)) {
      count
    } else {
      if (listOfIntegers(enterPoint) * listOfIntegers(enterPoint + 1) < 0) {
        foolRecursionSolution(listOfIntegers, enterPoint + 1, count + 1)
      } else {
        foolRecursionSolution(listOfIntegers, enterPoint + 1, count)
      }
    }
  }

  "simple test" should "" in {
    val in = List(1,-2,3,0,3,4,-2,-2,-3,1)
    println("number of sign changes: " + foolInterviewSolution(in))
  }

  "simple test 2" should "" in {
    val in = List(1,-2,3,0,3,4,-2,-2,-3,1)
    println("number of sign changes: " + foolRecursionSolution(in, 0, 0))
  }

  "simple test 3" should "" in {
    val in = List(1,-2,3,0,3,4,-2,-2,-3,1)
    println("number of sign changes: " + wellSolution(in))
  }

   //  На вход Seq[Future[String]]
   //  Получить Future[(Seq[String], Seq[Throwable]) - результат агрегации выполненых Future и исключений
  def futuresSolution(futures: Seq[Future[String]]): Future[(Seq[String], Seq[Throwable])] = {
    val excSeq: Seq[Future[Either[Throwable, String]]] = futures.map((futStr: Future[String]) => futStr.transform{
      case Success(value) => Success(Right(value))
      case Failure(exception) => Success(Left(exception))
    })
    val resSeq: Future[Seq[Either[Throwable, String]]] = Future.sequence(excSeq)
    val result: Future[(Seq[String], Seq[Throwable])] = resSeq.map(col => (col.collect {
      case Right(value) => value
    }, col.collect {
      case Left(exception) => exception
    }))
    result
  }


}
