package com.tsaplin.leetcode.search_in_rotated_sorted_array

import org.scalatest.FlatSpec

class SearchInRotatedSortedArray extends FlatSpec {

  //  You are given an integer array nums sorted in ascending order, and an integer target.
  //
  //  Suppose that nums is rotated at some pivot unknown to you beforehand (i.e., [0,1,2,4,5,6,7] might become [4,5,6,7,0,1,2]).
  //
  //  If target is found in the array return its index, otherwise, return -1.

  object Solution {

    // Runtime: 480 ms, faster than 79.07% of Scala online submissions for Search in Rotated Sorted Array.
    // Memory Usage: 50.7 MB, less than 62.79% of Scala online submissions for Search in Rotated Sorted Array.

    def simpleDirectWay(array: Array[Int], target: Int): Int = {
      var res = -1
      for (i <- array.indices) {
        if (target == array(i)) {
          res = i
        }
      }
      res
    }

    // Runtime: 476 ms, faster than 88.37% of Scala online submissions for Search in Rotated Sorted Array.
    // Memory Usage: 50.9 MB, less than 34.88% of Scala online submissions for Search in Rotated Sorted Array.

    def inlineDecision(array: Array[Int], target: Int): Int = {
      array.zipWithIndex.find(elemIndex => elemIndex._1 == target).getOrElse((-1, -1))._2
    }

    // Runtime: 468 ms, faster than 97.67% of Scala online submissions for Search in Rotated Sorted Array.
    // Memory Usage: 50.9 MB, less than 34.88% of Scala online submissions for Search in Rotated Sorted Array.

    def binaryFastestDecision(array: Array[Int], target: Int): Int = {
      @scala.annotation.tailrec
      def copyPastedRecursiveBinarySearch(array: Array[Int], target: Int, start: Int = 0, end: Int = array.length - 1): Int = {
        // If element not found
        if (start > end)
          return -1
        // Getting the middle element
        val middle = start + (end - start) / 2
        // If element found
        if (array(middle) == target)
          middle
        // Searching in the left half
        else if (array(middle) > target)
          copyPastedRecursiveBinarySearch(array, target, start, middle - 1)
        // Searching in the right half
        else
          copyPastedRecursiveBinarySearch(array, target, middle + 1, end)
      }
      @scala.annotation.tailrec
      def searchRotation(start: Int, end: Int): Int = {
        if (start <= end) {
          val pivot = start + ((end - start) / 2)
          if (pivot > 0 && array(pivot) < array(pivot - 1)) pivot
          else if (array(pivot) > array(end)) searchRotation(pivot + 1, end)
          else searchRotation(start, pivot - 1)
        } else 0
      }
      val rotationPoint = searchRotation(0, array.length - 1)
      val firstPartSearchResult = copyPastedRecursiveBinarySearch(array, target, 0, rotationPoint - 1)
      val secondPartSearchResult = copyPastedRecursiveBinarySearch(array, target, rotationPoint, array.length - 1)
      if (firstPartSearchResult != -1) {
        firstPartSearchResult
      } else if (secondPartSearchResult != -1) {
        secondPartSearchResult
      } else {
        -1
      }
    }

  }

  "test 1" should "be successful" in {
    val testArrayOne: Array[Int] = Array(4,5,6,7,0,1,2)
    val posSimple = Solution.simpleDirectWay(testArrayOne, 7)
    val posInline = Solution.inlineDecision(testArrayOne, 7)
    val posBinary = Solution.binaryFastestDecision(testArrayOne, 7)
    assert(posSimple == 3)
    assert(posInline == 3)
    assert(posBinary == 3)
  }

  "test 2" should "be successful" in {
    val testArrayTwo: Array[Int] = Array(4,6,7,0,3)
    val posSimple = Solution.simpleDirectWay(testArrayTwo, 2)
    val posInline = Solution.inlineDecision(testArrayTwo, 2)
    val posBinary = Solution.binaryFastestDecision(testArrayTwo, 2)
    assert(posSimple == -1)
    assert(posInline == -1)
    assert(posBinary == -1)
  }

  "test 3" should "be successful" in {
    val testArrayThree: Array[Int] = Array(4,6,7,0,2)
    val posSimple = Solution.simpleDirectWay(testArrayThree, 2)
    val posInline = Solution.inlineDecision(testArrayThree, 2)
    val posBinary = Solution.binaryFastestDecision(testArrayThree, 2)
    assert(posSimple == 4)
    assert(posInline == 4)
    assert(posBinary == 4)
  }

  "test 4" should "be successful" in {
    val testArrayOne: Array[Int] = Array(4,5,6,7,0,1,2)
    val posSimple = Solution.simpleDirectWay(testArrayOne, 0)
    val posInline = Solution.inlineDecision(testArrayOne, 0)
    val posBinary = Solution.binaryFastestDecision(testArrayOne, 0)
    assert(posSimple == 4)
    assert(posInline == 4)
    assert(posBinary == 4)
  }

}
