package com.tsaplin.leetcode.longest_polindromic

import org.scalatest.FlatSpec

import scala.collection.mutable.ListBuffer

class LongestPalindromicString extends FlatSpec {

  object Solution {
    def longestPalindrome(s: String): String = {
      val isOdd = s.length % 2 != 0
      val halfOfString = s.length / 2
      val centralIndex = if (isOdd) halfOfString + 1 else halfOfString
      val listBuffer = new ListBuffer[String]()
      for (currentIndex <- 0 to s.length) {
        if (currentIndex > centralIndex) {
          listBuffer.append(attemptToFindPalindrome(s, currentIndex, s.length - currentIndex - 1, new StringBuilder()))
        } else {
          listBuffer.append(attemptToFindPalindrome(s, currentIndex, currentIndex, new StringBuilder()))
        }
      }
      listBuffer.maxBy(_.length)
    }
    def attemptToFindPalindrome(s: String, index: Int, deep: Int, stringBuilder: StringBuilder): String = {
      for (curDeep <- 0 to deep) {
        val indexChar = s.charAt(index)
        if (curDeep == 0) {
          stringBuilder.append(indexChar)
        } else {
          val leftChar = s.charAt(index - curDeep)
          val rightChar = s.charAt(index + curDeep)
          if (leftChar.equals(rightChar)) {
            stringBuilder.insert(0, leftChar)
            stringBuilder.append(rightChar)
          } else if (indexChar.equals(leftChar) && curDeep == 1 && !indexChar.equals(rightChar)) {
            stringBuilder.insert(0, leftChar)
            return stringBuilder.mkString
          } else {
            return stringBuilder.mkString
          }
        }
      }
      stringBuilder.mkString
    }
  }

  "sandbox with string operations testing" should "demonstrate regular working with strings" in {

  }

  "test 0" should "be successful" in {
    val res = Solution.longestPalindrome("babad")
    println(res)
    assert(res.equals("bab") || res.equals("aba"))
  }

  "test 1" should "be successful" in {
    val res = Solution.longestPalindrome("cbbd")
    println(res)
    assert(res.equals("bb"))
  }

  "test 2" should "be successful" in {
    val res = Solution.longestPalindrome("cbbbd")
    println(res)
    assert(res.equals("bbb"))
  }



}
