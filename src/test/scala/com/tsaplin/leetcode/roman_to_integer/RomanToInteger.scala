package com.tsaplin.leetcode.roman_to_integer

import org.scalatest.FlatSpec

class RomanToInteger extends FlatSpec {

  object Solution {
    private val romans = Map('I' -> 1, 'V' -> 5, 'X' -> 10, 'L' -> 50, 'C' -> 100, 'D' -> 500, 'M' -> 1000)

    def romanToInt(s: String): Int = {
      s.map(c => romans.getOrElse(c, 0)).toList.reverse.reduce((cur: Int, next: Int) => (cur, next) match {
        case (cur: Int, next: Int) if next * 3 < cur => cur - next
        case (cur: Int, next: Int) => cur + next
        case (_, _) => 0
      })
    }
  }

  "test 0" should "be successful" in {
    val romanResult = Solution.romanToInt("MMMDCCXLIX")
    println(romanResult)
    assert(romanResult.equals(3749))
  }


  "test 1" should "be successful" in {
    val romanResult = Solution.romanToInt("LVIII")
    println(romanResult)
    assert(romanResult.equals(58))
  }

  "test 2" should "be successful" in {
    val romanResult = Solution.romanToInt("MCMXCIV")
    println(romanResult)
    assert(romanResult.equals(1994))
  }


}
