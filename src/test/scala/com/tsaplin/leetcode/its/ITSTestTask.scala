package com.tsaplin.leetcode.its

import org.scalatest.FlatSpec

import scala.collection.immutable.HashMap

class ITSTestTask extends FlatSpec {

  //  Write an algorithm that given a string containing any combination of the characters
  //  '(', ')', '{', '}', '[' and ']', determines if the input string is valid.
  //  An input string is valid when:
  //  - Opening parentheses are closed by the same type of closing parentheses, and
  //  - Opening parentheses are closed in the correct order.
  //  No other characters are present in the string except those mentioned above.
  //  Examples:
  //  1. "()" -> true
  //  2. "()[]{}" -> true
  //  3. "(]" -> false
  //  4. ")(" -> false
  //  5. "([)]" -> false
  //  6. "{[]}" -> true
  //  7. ")" -> false
  //  8. "([]" -> false
  //  9. "{" -> false
  //  10. "([)])" -> false
  //  11. "(((((((((())))))))))" -> true
  //  12. "" -> true
  //  Note that an empty string is also considered valid.

  object Solution {

    val bracketsMatches: HashMap[Char, Char] = scala.collection.immutable.HashMap[Char, Char]('{' -> '}', '[' -> ']', '(' -> ')')

    def checkStringForValidity(str: String): Boolean = {
      val chrArr = str.toCharArray
      if (str.length == 1) {
        return false
      } else if (str.length == 0) {
        return true
      } else {
        val strChr = chrArr(0)
        if ((strChr == '}') || (strChr == ')') || (strChr == ']')) {
          return false
        } else {
          val bracketMatch = bracketsMatches(strChr) // TODO fix when str can contains any other symbols instead of {, [, (, }, ], )
          if (str.contains(bracketMatch)) {
            val endOfStartBracket = str.lastIndexOf(bracketMatch)
            if (endOfStartBracket == 1 && str.length == 2) {
              return true
            } else {
              val innerBracketsStr = str.substring(1, endOfStartBracket)
              val firstBracketsConclusionResults = checkStringForValidity(innerBracketsStr)
              if (endOfStartBracket != str.length - 1) {
                val nextBracketsStr = str.substring(endOfStartBracket + 1, str.length)
                return firstBracketsConclusionResults && checkStringForValidity(nextBracketsStr)
              }
            }
          } else {
            return false
          }
        }
      }
      false
    }

    @scala.annotation.tailrec
    def regularCheckStringForValidity(str: String): Boolean = {
      val afterReplacementStr = str.replace("()", "").replace("{}", "").replace("[]", "")
      if (afterReplacementStr.isEmpty) return true
      if (afterReplacementStr.length == str.length) return false
      regularCheckStringForValidity(afterReplacementStr)
    }


  }

  "test 0" should "be successful" in {
    val str = "abcdefg"
    println(str.substring(1, str.length))
  }

  "test 1" should "be successful" in {
    val str = "abcdefg"
    println(str.lastIndexOf("g"))
  }

  "test for function" should "be successful 1" in {
   assert(Solution.checkStringForValidity("()"))
  }

  "test for function" should "be successful 2" in {
    assert(!Solution.checkStringForValidity("(()"))
  }

  "test for function" should "be successful 3" in {
    assert(!Solution.checkStringForValidity("(()))"))
  }

  "test for function" should "be successful 4" in {
    assert(Solution.checkStringForValidity("(){}"))
  }

  "test for function" should "be successful 5" in {
    assert(Solution.checkStringForValidity("(){}[]"))
  }

  "test for function" should "be successful 6" in {
    assert(!Solution.checkStringForValidity("(){}[[}"))
  }

  "test for function" should "be successful 7" in {
    assert(!Solution.checkStringForValidity("(){}[[]"))
  }

  "test for regular loop solution" should "be successful 1" in {
    assert(Solution.regularCheckStringForValidity("(){}[]"))
  }

  "test for regular loop solution" should "be successful 2" in {
    assert(Solution.regularCheckStringForValidity("(()){}[]"))
  }

  "test for regular loop solution" should "be successful 3" in {
    assert(Solution.regularCheckStringForValidity("(()()){}[]()"))
  }

  "test for regular loop solution" should "be successful 4" in {
    assert(!Solution.regularCheckStringForValidity("((){}[]"))
  }

  "test for regular loop solution" should "be successful 5" in {
    assert(!Solution.regularCheckStringForValidity("(){(}[]"))
  }

  "test for regular loop solution" should "be successful 6" in {
    assert(Solution.regularCheckStringForValidity(""))
  }

}


