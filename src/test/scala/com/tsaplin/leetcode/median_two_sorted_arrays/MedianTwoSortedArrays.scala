package com.tsaplin.leetcode.median_two_sorted_arrays

import org.scalatest.FlatSpec

class MedianTwoSortedArrays extends FlatSpec {

  // There are two sorted arrays nums1 and nums2 of size m and n respectively.
  // Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).
  // You may assume nums1 and nums2 cannot be both empty.
  //
  // Example 1:
  // nums1 = [1, 3]
  // nums2 = [2]
  // The median is 2.0
  //
  // Example 2:
  // nums1 = [1, 2]
  // nums2 = [3, 4]
  // The median is (2 + 3)/2 = 2.5

  object Solution {

    // Runtime: 424 ms, faster than 90.12% of Scala online submissions for Median of Two Sorted Arrays.
    // Memory Usage: 55.4 MB, less than 100.00% of Scala online submissions for Median of Two Sorted Arrays.

    def mergeSortedArrays(nums1: Array[Int], nums2: Array[Int]): Array[Int] = {
      val size1 = nums1.length
      val size2 = nums2.length
      val size3 = size1 + size2
      val nums3: Array[Int] = new Array[Int](size3)
      var index1 = 0
      var index2 = 0
      var index3 = 0
      while (index1 < size1 && index2 < size2) {
        if (nums1(index1) < nums2(index2)) {
          nums3(index3) = nums1(index1)
          index1 = index1 + 1
        } else if (nums1(index1) > nums2(index2)){
          nums3(index3) = nums2(index2)
          index2 = index2 + 1
        } else {
          nums3(index3) = nums1(index1)
          index1 = index1 + 1
          index3 = index3 + 1
          nums3(index3) = nums2(index2)
          index2 = index2 + 1
        }
        index3 = index3 + 1
      }
      while (index1 < size1) {
        nums3(index3) = nums1(index1)
        index1 = index1 + 1
        index3 = index3 + 1
      }
      while (index2 < size2) {
        nums3(index3) = nums2(index2)
        index2 = index2 + 1
        index3 = index3 + 1
      }
      nums3
    }

    def findMedianSortedArrays(nums1: Array[Int], nums2: Array[Int]): Double = {
      val nums3 = mergeSortedArrays(nums1, nums2)
      val size3 = nums3.length
      val odd = size3 % 2 == 0
      if (odd) {
        val frstIndx = size3 / 2 - 1
        val scndIndx = size3 / 2
        val median = (nums3(frstIndx) + nums3(scndIndx)).doubleValue() / 2
        median
      } else {
        val frstIndx = size3 / 2
        val median = nums3(frstIndx)
        median
      }
    }
  }


  "test 1" should "be successful" in {
    val nums1: Array[Int] = Array(1, 3)
    val nums2: Array[Int] = Array(2)
    val merged: Array[Int] = Array(1, 2, 3)
    val nums3 = Solution.mergeSortedArrays(nums1, nums2)
    nums3.foreach(item => {
      System.out.println(item)
    })
    assert(merged.sameElements(nums3))
    val median = Solution.findMedianSortedArrays(nums1, nums2)
    System.out.println(median)
    assert(median.equals(2.0))
  }

  "test 2" should "be successful" in {
    val nums1: Array[Int] = Array(1, 3)
    val nums2: Array[Int] = Array(2, 4, 5 , 6, 7, 8)
    val merged: Array[Int] = Array(1, 2, 3, 4, 5 , 6, 7, 8)
    val nums3 = Solution.mergeSortedArrays(nums1, nums2)
    nums3.foreach(item => {
      System.out.println(item)
    })
    assert(merged.sameElements(nums3))
    val median = Solution.findMedianSortedArrays(nums1, nums2)
    System.out.println(median)
    assert(median.equals(4.5))
  }

  "test 3" should "be successful" in {
    val nums1: Array[Int] = Array(1, 2, 3)
    val nums2: Array[Int] = Array(1, 2, 3)
    val merged: Array[Int] = Array(1, 1, 2, 2, 3, 3)
    val nums3 = Solution.mergeSortedArrays(nums1, nums2)
    nums3.foreach(item => {
      System.out.println(item)
    })
    assert(merged.sameElements(nums3))
    val median = Solution.findMedianSortedArrays(nums1, nums2)
    System.out.println(median)
    assert(median.equals(2.0))
  }

  "test 4" should "be successful" in {
    val nums1: Array[Int] = Array(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 4)
    val nums2: Array[Int] = Array(1, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4)
    val merged: Array[Int] = Array(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4)
    val nums3 = Solution.mergeSortedArrays(nums1, nums2)
    nums3.foreach(item => {
      System.out.println(item)
    })
    assert(merged.sameElements(nums3))
    val median = Solution.findMedianSortedArrays(nums1, nums2)
    System.out.println(median)
    assert(median.equals(3.0))
  }


}