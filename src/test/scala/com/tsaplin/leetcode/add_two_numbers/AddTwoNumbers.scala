package com.tsaplin.leetcode.add_two_numbers

import org.scalatest.FlatSpec

class AddTwoNumbers extends FlatSpec {

  //  You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.
  //  You may assume the two numbers do not contain any leading zero, except the number 0 itself.
  //
  //  Example:
  //  Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
  //  Output: 7 -> 0 -> 8
  //  Explanation: 342 + 465 = 807.

  class ListNode(var _x: Int = 0) {
    var next: ListNode = null
    var x: Int = _x
  }

  object Solution {

    // Runtime: 360 ms, faster than 91.77% of Scala online submissions for Add Two Numbers.
    // Memory Usage: 57.8 MB, less than 100.00% of Scala online submissions for Add Two Numbers.

    def addTwoNumbers(l1: ListNode, l2: ListNode): ListNode = {
      val sum = l1.x + l2.x
      val l3x = sum % 10
      val addTen = sum >= 10
      if (l1.next != null && l2.next != null) {
        return nextStep(l3x, l1.next, l2.next, addTen)
      }
      if (l1.next != null && l2.next == null) {
        return nextStep(l3x, l1.next, new ListNode(0), addTen)
      }
      if (l1.next == null && l2.next != null) {
        return nextStep(l3x, new ListNode(0), l2.next, addTen)
      }
      if (l1.next == null && l2.next == null) {
        return nextStep(l3x, new ListNode(-1), new ListNode(-1), addTen)
      }
      new ListNode(0)
    }
    def nextStep(l3x: Int, l1n: ListNode, l2n: ListNode, addTen: Boolean): ListNode = {
      val result = new ListNode(l3x)
      if (addTen) {
        if (l1n.x == -1 && l2n.x == -1) {
          val l1nU = new ListNode(l1n.x + 2)
          val l2nU = new ListNode(l2n.x + 1)
          result.next = addTwoNumbers(l1nU, l2nU)
          return result
        }
        val l1nU = new ListNode(l1n.x + 1)
        l1nU.next = l1n.next
        result.next = addTwoNumbers(l1nU, l2n)
      } else {
        if (l1n.x == -1 && l2n.x == -1) {
          return result
        }
        result.next = addTwoNumbers(l1n, l2n)
      }
      result
    }
  }

  "add two numbers test" should "show me how much I clever 1" in {
    val l1 = new ListNode(2)
    l1.next = new ListNode(4)
    l1.next.next = new ListNode(3)
    val l2 = new ListNode(5)
    l2.next = new ListNode(6)
    l2.next.next = new ListNode(4)
    // 342 + 465 = 807
    val l3 = Solution.addTwoNumbers(l1, l2)
    val l3Str = l3.next.next.x.toString + l3.next.x.toString + l3.x.toString
    System.out.println(l3Str)
    assert(l3Str.equals("807"))
  }

  "add two numbers test" should "show me how much I clever 2" in {
    val l1 = new ListNode(2)
    l1.next = new ListNode(4)
    l1.next.next = new ListNode(0)
    val l2 = new ListNode(6)
    l2.next = new ListNode(4)
    l2.next.next = new ListNode(4)
    // 446 + 042 = 488
    val l3 = Solution.addTwoNumbers(l1, l2)
    val l3Str = l3.next.next.x.toString + l3.next.x.toString + l3.x.toString
    System.out.println(l3Str)
    assert(l3Str.equals("488"))
  }

  "add two numbers test" should "show me how much I clever 3" in {
    val l1 = new ListNode(2)
    l1.next = new ListNode(4)
    val l2 = new ListNode(6)
    l2.next = new ListNode(4)
    l2.next.next = new ListNode(4)
    // 446 + 42 = 488
    val l3 = Solution.addTwoNumbers(l1, l2)
    val l3Str = l3.next.next.x.toString + l3.next.x.toString + l3.x.toString
    System.out.println(l3Str)
    assert(l3Str.equals("488"))
  }

  "add two numbers test" should "show me how much I clever 4" in {
    val l1 = new ListNode(2)
    val l2 = new ListNode(6)
    l2.next = new ListNode(4)
    l2.next.next = new ListNode(4)
    // 446 + 2 = 448
    val l3 = Solution.addTwoNumbers(l1, l2)
    val l3Str = l3.next.next.x.toString + l3.next.x.toString + l3.x.toString
    System.out.println(l3Str)
    assert(l3Str.equals("448"))
  }


  "add two numbers test" should "show me how much I clever 5" in {
    val l1 = new ListNode(0)
    val l2 = new ListNode(5)
    l2.next = new ListNode(6)
    l2.next.next = new ListNode(4)
    // 0 + 465 = 465
    val l3 = Solution.addTwoNumbers(l1, l2)
    val l3Str = l3.next.next.x.toString + l3.next.x.toString + l3.x.toString
    System.out.println(l3Str)
    assert(l3Str.equals("465"))
  }

  "add two numbers test" should "show me how much I clever 6" in {
    val l1 = new ListNode(2)
    l1.next = new ListNode(4)
    l1.next.next = new ListNode(3)
    l1.next.next.next = new ListNode(9)
    val l2 = new ListNode(5)
    l2.next = new ListNode(6)
    l2.next.next = new ListNode(4)
    // 9342 + 465 = 807
    val l3 = Solution.addTwoNumbers(l1, l2)
    val l3Str = l3.next.next.next.x.toString + l3.next.next.x.toString + l3.next.x.toString + l3.x.toString
    System.out.println(l3Str)
    assert(l3Str.equals("9807"))
  }

  "add two numbers test" should "show me how much I clever 7" in {
    val l1 = new ListNode(5)
    val l2 = new ListNode(5)
    val l3 = Solution.addTwoNumbers(l1, l2)
    val l3Str = l3.next.x.toString + l3.x.toString
    System.out.println(l3Str)
    assert(l3Str.equals("10"))
  }

}
