package com.tsaplin.leetcode.add_two_numbers

import org.scalatest.FlatSpec

class AddTwoNumbersMyVersion extends FlatSpec {

  //  You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.
  //  You may assume the two numbers do not contain any leading zero, except the number 0 itself.
  //
  //  Example:
  //  Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
  //  Output: 7 -> 0 -> 8
  //  Explanation: 342 + 465 = 807.

  class ListNode(var _x: Option[Int] = None) {
    var next: Option[ListNode] = None
    var x: Option[Int] = _x
  }

  object Solution {

    def addTwoNumbers(l1: ListNode, l2: ListNode): ListNode = {
      val sum = l1.x.getOrElse(0) + l2.x.getOrElse(0)
      val l3x = sum % 10
      val addTen = sum >= 10
      if (l1.next.isEmpty && l2.next.isEmpty) {
        return nextStep(l3x, new ListNode(None), new ListNode(None), addTen)
      }
      nextStep(l3x, l1.next.getOrElse(new ListNode(Some(0))), l2.next.getOrElse(new ListNode(Some(0))), addTen)
    }

    def nextStep(l3x: Int, l1n: ListNode, l2n: ListNode, addTen: Boolean): ListNode = {
      val result = new ListNode(Some(l3x))
      if (l1n._x.isEmpty && l2n._x.isEmpty && !addTen) {
        return result
      }
      val l1nU = if (addTen) {
        val l1nR = new ListNode(Some(l1n.x.getOrElse(0) + 1))
        l1nR.next = l1n.next
        l1nR
      } else l1n
      result.next = Some(addTwoNumbers(l1nU, l2n))
      result
    }

  }

  "add two numbers test" should "show me how much I clever 1" in {
    val l1 = new ListNode(Some(2))
    l1.next = Some(new ListNode(Some(4)))
    l1.next.get.next = Some(new ListNode(Some(3)))
    val l2 = new ListNode(Some(5))
    l2.next = Some(new ListNode(Some(6)))
    l2.next.get.next = Some(new ListNode(Some(4)))
    // 342 + 465 = 807
    val l3 = Solution.addTwoNumbers(l1, l2)
    val l3Str = l3.next.get.next.get._x.get.toString + l3.next.get.x.get.toString + l3.x.get.toString
    System.out.println(l3Str)
    assert(l3Str.equals("807"))
  }

  "add two numbers test" should "show me how much I clever 2" in {
    val l1 = new ListNode(Some(2))
    l1.next = Some(new ListNode(Some(4)))
    val l2 = new ListNode(Some(5))
    l2.next = Some(new ListNode(Some(6)))
    l2.next.get.next = Some(new ListNode(Some(4)))
    // 42 + 465 = 507
    val l3 = Solution.addTwoNumbers(l1, l2)
    val l3Str = l3.next.get.next.get._x.get.toString + l3.next.get.x.get.toString + l3.x.get.toString
    System.out.println(l3Str)
    assert(l3Str.equals("507"))
  }

  "add two numbers test" should "show me how much I clever 3" in {
    val l1 = new ListNode(Some(0))
    l1.next = Some(new ListNode(Some(4)))
    val l2 = new ListNode(Some(0))
    l2.next = Some(new ListNode(Some(6)))
    l2.next.get.next = Some(new ListNode(Some(4)))
    // 40 + 460 = 500
    val l3 = Solution.addTwoNumbers(l1, l2)
    val l3Str = l3.next.get.next.get._x.get.toString + l3.next.get.x.get.toString + l3.x.get.toString
    System.out.println(l3Str)
    assert(l3Str.equals("500"))
  }

  "add two numbers test" should "show me how much I clever 4" in {
    val l1 = new ListNode(Some(0))
    val l2 = new ListNode(Some(0))
    l2.next = Some(new ListNode(Some(6)))
    l2.next.get.next = Some(new ListNode(Some(4)))
    // 0 + 460 = 460
    val l3 = Solution.addTwoNumbers(l1, l2)
    val l3Str = l3.next.get.next.get._x.get.toString + l3.next.get.x.get.toString + l3.x.get.toString
    System.out.println(l3Str)
    assert(l3Str.equals("460"))
  }

}
