package com.tsaplin.leetcode.longest_prefix

import org.scalatest.FlatSpec

class LongestPrefix extends FlatSpec {

  object Solution {
    def longestCommonPrefix(strs: Array[String]): String = {
      val minStr = strs.minBy(_.length)
      val minLength = minStr.length
      val resultStringBuilder = new StringBuilder
      for (i <- 0 until minLength) {
          val equalsSign: Array[Boolean] = strs.map(str => str.charAt(i).equals(minStr.charAt(i)))
          val allEquals = !equalsSign.contains(false)
          if (allEquals) {
            resultStringBuilder.append(minStr.charAt(i))
          } else return resultStringBuilder.mkString
      }
      resultStringBuilder.mkString
    }
  }

  "sandbox with string operations testing" should "demonstrate regular working with strings" in {

  }

  "test 0" should "be successful" in {
    val strs = Array("flower","flow","flight")
    val res = Solution.longestCommonPrefix(strs)
    println(res)
    assert(res.equals("fl"))
  }

  "test 1" should "be successful" in {
    val strs = Array("dog","racecar","car")
    val res = Solution.longestCommonPrefix(strs)
    println(res)
    assert(res.equals(""))
  }

  //  SCALA 3 at leetcode compiler
  //  import scala.util.boundary, boundary.break
  //
  //  object Solution {
  //    def longestCommonPrefix(strs: Array[String]): String = {
  //      val minStr = strs.minBy(_.length)
  //      val minLength = minStr.length
  //      val resultStringBuilder = new StringBuilder
  //      boundary:
  //        for (i <- 0 until minLength) {
  //            val equalsSign: Array[Boolean] = strs.map(str => str.charAt(i).equals(minStr.charAt(i)))
  //            val allEquals = !equalsSign.contains(false)
  //            if (allEquals) {
  //                resultStringBuilder.append(minStr.charAt(i))
  //            } else break(resultStringBuilder.mkString)
  //        }
  //      resultStringBuilder.mkString
  //    }
  //  }


}
