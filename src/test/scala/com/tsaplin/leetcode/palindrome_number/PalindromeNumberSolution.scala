package com.tsaplin.leetcode.palindrome_number

import org.scalatest.FlatSpec

class PalindromeNumberSolution extends FlatSpec {

  object Solution {
    def isPalindrome(x: Int): Boolean = {
      val xStr = x.toString
      val reversedXStr = xStr.reverse
      xStr.equals(reversedXStr)
    }
  }

  "test 0" should "be successful" in {
    val number = 121
    val res = Solution.isPalindrome(number)
    println(res)
    assert(res)
  }

  "test 1" should "be successful" in {
    val number = -121
    val res = Solution.isPalindrome(number)
    println(res)
    assert(!res)
  }

  "test 2" should "be successful" in {
    val number = 10
    val res = Solution.isPalindrome(number)
    println(res)
    assert(!res)
  }

}
