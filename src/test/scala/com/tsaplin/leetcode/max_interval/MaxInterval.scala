package com.tsaplin.leetcode.max_interval

import org.scalatest.FlatSpec

class MaxInterval extends FlatSpec {

  // Дана строка из нулей и единиц; нужно определить длину максимального интервала из единиц при условии, что можно удалить не более одного нуля из исходной строки.
  //
  //  101010 → 2
  //  10111011 → 5
  //  11000110 → 2
  //  01101001010 → 3


  def maxInterval(str: String): Unit = {
    val zipped = str.toCharArray.toList.zipWithIndex.map(charIndex => charIndex -> str) // получаем из строчки массив структур типа: (символ, его индекс) - исходная строка
      .filter(charIndexString => charIndexString._1._1.equals('0')) // нас интересует только символ 0, потому что удалить мы можем лишь его и лишь однажды, значит оставляем структуры с символом 0
      .map(charIndexString => {
        val fS = charIndexString._2.splitAt(charIndexString._1._2)
        fS._1 + fS._2.tail
      }) // здесь мы убираем по одному 0 из каждой последовательности нулей, здесь уже имеем такой массив List(1101001010, 0111001010, 0110101010, 0110101010, 0110100110, 0110100101) для исходной строки 01101001010
    val intervals = zipped.map(str => str.split("0").map(str => str.length).max) // здесь мы считаем различные получившиеся интервалы
    val interval = intervals.max // здесь выбираем максимальный интервал
    println(interval)
  }

  "test solution" should "" in {

    maxInterval("01101001010") // should be 3

    maxInterval("10111011") // should be 5

    maxInterval("101010") // should be 2

    maxInterval("11000110") // should be 2


  }

}
