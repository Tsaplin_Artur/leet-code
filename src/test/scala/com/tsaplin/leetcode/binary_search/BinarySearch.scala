package com.tsaplin.leetcode.binary_search

import org.scalatest.FlatSpec

class BinarySearch extends FlatSpec {

  // https://leetcode.com/problems/binary-search/
  //
  // Given a sorted (in ascending order) integer array nums of n elements and a target value, write a function to search target in nums. If target exists, then return its index, otherwise return -1.
  //
  //
  // Example 1:
  //
  // Input: nums = [-1,0,3,5,9,12], target = 9
  // Output: 4
  // Explanation: 9 exists in nums and its index is 4
  //
  // Example 2:
  //
  // Input: nums = [-1,0,3,5,9,12], target = 2
  // Output: -1
  // Explanation: 2 does not exist in nums so return -1
  //
  //
  // Note:
  //
  // You may assume that all elements in nums are unique.
  // n will be in the range [1, 10000].
  // The value of each element in nums will be in the range [-9999, 9999].

  object Solution {
    def loopSearch(array: Array[Int], target: Int): Int = {
      var start = 0
      var end = array.length - 1
      var result = -1
      while (start <= end) {
        val middle = start + (end - start) / 2
        if (array(middle) == target) {
          result = middle
          end = - 1
        } else if (array(middle) > target) {
          end = middle - 1
        } else {
          start = middle + 1
        }
      }
      result
    }
    @scala.annotation.tailrec
    def recursiveBinarySearch(array: Array[Int], target: Int, start: Int = 0, end: Int): Int = {
      // If element not found
      if (start > end) return -1
      // Getting the middle element
      val middle = start + (end - start) / 2
      // If element found
      if (array(middle) == target) middle
      // Searching in the left half
      else if (array(middle) > target) recursiveBinarySearch(array, target, start, middle - 1)
      // Searching in the right half
      else recursiveBinarySearch(array, target, middle + 1, end)
    }
    def searchRecursively(nums: Array[Int], target: Int): Int = {
      val end = nums.length - 1
      recursiveBinarySearch(nums, target, 0, end)
    }
  }

  "test 0" should "be successful" in {
    val testArrayOne: Array[Int] = Array(0,1,2,4,5,6,7)
    val existingTarget = 5
    val nonExistingTarget = 3
    assert(Solution.searchRecursively(testArrayOne, existingTarget) == 4)
    assert(Solution.searchRecursively(testArrayOne, nonExistingTarget) == -1)
    assert(Solution.loopSearch(testArrayOne, existingTarget) == 4)
    assert(Solution.loopSearch(testArrayOne, nonExistingTarget) == -1)
  }

  "test 1" should "be successful" in {
    val testArrayOne: Array[Int] = Array()
    val nonExistingTarget = 3
    assert(Solution.searchRecursively(testArrayOne, nonExistingTarget) == -1)
    assert(Solution.loopSearch(testArrayOne, nonExistingTarget) == -1)
  }

  "test 2" should "be successful" in {
    val testArrayOne: Array[Int] = Array(1, 2)
    val nonExistingTarget = 3
    val existingTarget = 2
    assert(Solution.searchRecursively(testArrayOne, nonExistingTarget) == -1)
    assert(Solution.searchRecursively(testArrayOne, existingTarget) == 1)
    assert(Solution.loopSearch(testArrayOne, nonExistingTarget) == -1)
    assert(Solution.loopSearch(testArrayOne, existingTarget) == 1)
  }

  "test 3" should "be successful" in {
    val testArrayOne: Array[Int] = Array(1)
    val nonExistingTarget = 2
    val existingTarget = 1
    assert(Solution.searchRecursively(testArrayOne, nonExistingTarget) == -1)
    assert(Solution.searchRecursively(testArrayOne, existingTarget) == 0)
    assert(Solution.loopSearch(testArrayOne, nonExistingTarget) == -1)
    assert(Solution.loopSearch(testArrayOne, existingTarget) == 0)
  }

}
