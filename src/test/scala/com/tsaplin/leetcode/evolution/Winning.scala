package com.tsaplin.leetcode.evolution

import org.scalatest.FlatSpec

import scala.collection.mutable.ListBuffer

case class Winning(userID: Int, amount: Double)

trait WinningManager {

  def winningsCache: WinningsCache

  def winningInformer: WinningInformer

  def putWinning(item: Winning)

  def putWinnings(items: Seq[Winning])

  def getWinnings: Seq[Winning]

  def getPlayerWinnings(userID: Int): Seq[Winning]

}

trait WinningsCache {

  def getWinnings: Seq[Winning]

  def putWinning(item: Winning)

  def putWinnings(items: Seq[Winning])

}

trait WinningInformer {

  def winningInform(winning: Winning)

}

case class DefaultWinningCache() extends WinningsCache {

  val winningsList: ListBuffer[Winning] = ListBuffer[Winning]()

  override def putWinning(item: Winning): Unit = winningsList += (item)

  override def getWinnings: Seq[Winning] = winningsList

  override def putWinnings(items: Seq[Winning]): Unit = winningsList ++= (items)

}

class DefaultWinningInformer extends WinningInformer {

  override def winningInform(winning: Winning): Unit = println(s"Player with ID: ${winning.userID} have a won with amount: ${winning.amount}.")

}

class DefaultWinningManager extends WinningManager {

  val winningsCache: WinningsCache = DefaultWinningCache()

  val winningInformer: WinningInformer = new DefaultWinningInformer

  override def putWinning(item: Winning): Unit = {
    winningsCache.putWinning(item)
    winningInformer.winningInform(item)
  }

  override def putWinnings(items: Seq[Winning]): Unit = {
    winningsCache.putWinnings(items)
    items.foreach(itm => winningInformer.winningInform(itm))
  }

  override def getWinnings: Seq[Winning] = winningsCache.getWinnings

  override def getPlayerWinnings(userID: Int): Seq[Winning] = winningsCache.getWinnings.filter(itm => itm.userID == userID)

  def getMaxTenPlayerWinnings(userID: Int): Seq[Winning] = {
    val cacheSize = winningsCache.getWinnings.size
    val edge = if (cacheSize > 10) 10 else cacheSize - 1
    winningsCache.getWinnings.filter(itm => itm.userID == userID).sortBy(itm => itm.amount)(Ordering.Double.reverse).slice(0, edge)
  }

}

class WinningTest extends FlatSpec {

  "winning test" should " " in {
    val winningManager = new DefaultWinningManager
    val testWinningCache = Seq(Winning(1, 1.0), Winning(1, 1.5), Winning(2, 2.0), Winning(3, 1.0), Winning(3, 5.0))
    winningManager.putWinnings(testWinningCache)
    println(winningManager.getMaxTenPlayerWinnings(1).mkString(", "))
    println(winningManager.getMaxTenPlayerWinnings(3).mkString(", "))
  }

}
