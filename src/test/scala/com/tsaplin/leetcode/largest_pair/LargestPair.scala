package com.tsaplin.leetcode.largest_pair

import org.scalatest.FlatSpec

class LargestPair extends FlatSpec {

  // Have the function Largest Pair (num) take the num parameter being passed and determine the largest double digit
  // number within the whole number. For example: if num is 4759472 then your program should return 94 because that
  // is the largest double digit number. The input will always contain at least two positive digits.
  // Examples:
  // Input: 453857
  // Output: 85
  // Input: 363223311
  // Output: 63

  def largestPair(num: Int): Int = {
    val numString = num.toString
    val numSize = numString.length
    val pairsArray = new Array[Int](numSize)
    for (i <- 0 until numSize) {
      val pairDigit = numString.slice(i, i + 2).toInt
      pairsArray(i) = pairDigit
      println(s"index $i, pair $pairDigit")
    }
    pairsArray.toList.max
  }

  "test 0" should "be successful" in {
    val num1 = 453857
    val res1 = largestPair(num1)
    assert(res1.equals(85))

    val num2 = 363223311
    val res2 = largestPair(num2)
    assert(res2.equals(63))
  }



}
