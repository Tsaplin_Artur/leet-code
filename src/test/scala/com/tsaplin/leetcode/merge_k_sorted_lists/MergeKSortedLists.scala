package com.tsaplin.leetcode.merge_k_sorted_lists

import org.scalatest.FlatSpec

class MergeKSortedLists extends FlatSpec {

  // https://leetcode.com/problems/merge-k-sorted-lists/
  //
  // You are given an array of k linked-lists lists, each linked-list is sorted in ascending order.
  //
  // Merge all the linked-lists into one sorted linked-list and return it.
  //
  // Example 1:
  //
  // Input: lists = [[1,4,5],[1,3,4],[2,6]]
  // Output: [1,1,2,3,4,4,5,6]
  // Explanation: The linked-lists are:
  // [
  //   1->4->5,
  //   1->3->4,
  //   2->6
  // ]
  // merging them into one sorted list:
  // 1->1->2->3->4->4->5->6
  //

  class ListNode(_x: Int = 0, _next: ListNode = null) {
    var next: ListNode = _next
    var x: Int = _x
  }

  object Solution {

    // Runtime: 1576 ms, faster than 10.00% of Scala online submissions for Merge k Sorted Lists.
    // Memory Usage: 473.5 MB, less than 8.00% of Scala online submissions for Merge k Sorted Lists.

    def mergeKLists(lists: Array[ListNode]): ListNode = {
      def mergeListNodes(startNode: ListNode, listNodeOne: ListNode, listNodeTwo: ListNode): ListNode = {
        if (listNodeOne == null && listNodeTwo == null) {
          return null
        }
        if (listNodeOne == null && listNodeTwo != null) {
          startNode.x = listNodeTwo.x
          startNode.next = mergeListNodes(new ListNode(), null, listNodeTwo.next)
        } else if (listNodeOne != null && listNodeTwo == null) {
          startNode.x = listNodeOne.x
          startNode.next = mergeListNodes(new ListNode(), listNodeOne.next, null)
        } else if (listNodeOne != null && listNodeTwo != null) {
          if (listNodeOne.x >= listNodeTwo.x) {
            startNode.x = listNodeTwo.x
            startNode.next = mergeListNodes(new ListNode(), listNodeOne, listNodeTwo.next)
          } else {
            startNode.x = listNodeOne.x
            startNode.next = mergeListNodes(new ListNode(), listNodeOne.next, listNodeTwo)
          }
        }
        startNode
      }
      lists.reduceOption((x, y) => mergeListNodes(new ListNode, x, y)).orNull
    }
  }

  "test 0" should "be successful" in {
    // Input: lists = [[1,4,5],[1,3,4]]
    // Output: [1,1,3,4,4,5]
    val listOne = new ListNode(1, new ListNode(4, new ListNode(5)))
    val listTwo = new ListNode(1, new ListNode(3, new ListNode(4)))
    val res: ListNode = Solution.mergeKLists(Array(listOne, listTwo))
    @scala.annotation.tailrec
    def printNode(node: ListNode): Unit = {
      print(node.x)
      if (node.next != null) {
        print(", ")
        printNode(node.next)
      } else {
        print(".")
      }
    }
    printNode(res)
  }

  "test 1" should "be successful" in {
    // Input: lists = [[1,4,5],[1,3,4],[2,6]]
    // Output: [1,1,2,3,4,4,5,6]
    val listOne = new ListNode(1, new ListNode(4, new ListNode(5)))
    val listTwo = new ListNode(1, new ListNode(3, new ListNode(4)))
    val listThree = new ListNode(2, new ListNode(6))
    val res: ListNode = Solution.mergeKLists(Array(listOne, listTwo, listThree))
    @scala.annotation.tailrec
    def printNode(node: ListNode): Unit = {
      print(node.x)
      if (node.next != null) {
        print(", ")
        printNode(node.next)
      } else {
        print(".")
      }
    }
    printNode(res)
  }

  "test 2" should "be successful" in {
    // Input: lists = [[1, 1, 3, 4, 4, 5],[2,6]]
    // Output: [1,1,2,3,4,4,5,6]
    val listOne = new ListNode(1, new ListNode(1, new ListNode(3, new ListNode(4, new ListNode(4, new ListNode(5))))))
    val listTwo = new ListNode(2, new ListNode(6))
    val res = Solution.mergeKLists(Array(listOne, listTwo))
    @scala.annotation.tailrec
    def printNode(node: ListNode): Unit = {
      print(node.x)
      if (node.next != null) {
        print(", ")
        printNode(node.next)
      } else {
        print(".")
      }
    }
    printNode(res)
  }

  "test 4" should "be successful" in {
    // Input: lists = [[7,9,15],[1,6,8]]
    // Output: [1,6,7,8,9,15]
    val listOne = new ListNode(7, new ListNode(9, new ListNode(15)))
    val listTwo = new ListNode(1, new ListNode(6, new ListNode(8)))
    val res: ListNode = Solution.mergeKLists(Array(listOne, listTwo))
    @scala.annotation.tailrec
    def printNode(node: ListNode): Unit = {
      print(node.x)
      if (node.next != null) {
        print(", ")
        printNode(node.next)
      } else {
        print(".")
      }
    }
    printNode(res)
  }

  "test 5" should "be successful" in {
    // Input: lists = [[2,6], [1, 1, 3, 4, 4, 5], [7,10,15]]
    // Output: [1,1,2,3,4,4,5,6,7,10,15]
    val listOne = new ListNode(2, new ListNode(6))
    val listTwo = new ListNode(1, new ListNode(1, new ListNode(3, new ListNode(4, new ListNode(4, new ListNode(5))))))
    val listThree = new ListNode(7, new ListNode(10, new ListNode(15)))
    val res = Solution.mergeKLists(Array(listOne, listTwo, listThree))
    @scala.annotation.tailrec
    def printNode(node: ListNode): Unit = {
      print(node.x)
      if (node.next != null) {
        print(", ")
        printNode(node.next)
      } else {
        print(".")
      }
    }
    printNode(res)
  }

  "test 6" should "be successful" in {
    // Input: lists = [[0], [0], [0]]
    // Output: [0, 0, 0]
    val listOne = new ListNode()
    val listTwo = new ListNode()
    val listThree = new ListNode()
    val res = Solution.mergeKLists(Array(listOne, listTwo, listThree))
    @scala.annotation.tailrec
    def printNode(node: ListNode): Unit = {
      print(node.x)
      if (node.next != null) {
        print(", ")
        printNode(node.next)
      } else {
        print(".")
      }
    }
    printNode(res)
  }

  "test 7" should "be successful" in {
    // Input: lists = []
    // Output: []
    val res = Solution.mergeKLists(Array())
    assert(res == null)
  }

}
