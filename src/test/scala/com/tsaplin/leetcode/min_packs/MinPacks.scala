package com.tsaplin.leetcode.min_packs

import org.scalatest.FlatSpec

import scala.collection.mutable

class MinPacks extends FlatSpec {

  //  Imagine for a moment that one of our product lines ships in various pack sizes:
  //•	250 Items
  //•	500 Items
  //•	1000 Items
  //•	2000 Items
  //•	5000 Items
  //  Our customers can order any number of these items through our website, but they will always only be given complete packs.
  //  1.	Only whole packs can be sent. Packs cannot be broken open.
  //  2.	Within the constraints of Rule 1 above, send out no more items than necessary to fulfil the order.
  //  3.	Within the constraints of Rules 1 & 2 above, send out as few packs as possible to fulfil each order.
  //  (Please note, rule #2 takes precedence over rule #3)
  //  So, for example:
  //  Items ordered 	Correct number of packs 	Incorrect number of packs
  //  1 	            1 x 250 	                1 x 500 – more items than necessary
  //  -----------------------------------------------------------------------------
  //  250 	          1 x 250 	                1 x 500 – more items than necessary
  //  -----------------------------------------------------------------------------
  //  251 	          1 x 500 	                2 x 250 – more packs than necessary
  //  -----------------------------------------------------------------------------
  //  501 	          1 x 500                   1 x 1000 – more items than necessary
  //                  1 x 250 	                3 x 250 – more packs than necessary
  //  -----------------------------------------------------------------------------
  //  12001 	        2 x 5000                  3 x 5000 – more items than necessary
  //                  1 x 2000
  //                  1 x 250
  //  -----------------------------------------------------------------------------
  //
  //  To further illustrate the rules above, please consider this custom pack size example:
  //•	23 Items
  //•	31 Items
  //•	53 Items
  //
  //  Items Order: 263
  //  Correct Number of packs: 2x23, 7x31
  //  Incorrect answer: 5x53

  def recursiveMinPacksPart(varietyOfPacksDescSorted: Seq[Int], totalItems: Int, currentPackIndex: Int = 0, resultMap: Map[Int, Int] = Map()): Map[Int, Int] = {
    val varietyOfPacksSize = varietyOfPacksDescSorted.size
    if (currentPackIndex.equals(varietyOfPacksSize - 1)) {
      val minimalPack = varietyOfPacksDescSorted(currentPackIndex)
      val zeroOrOne = if (totalItems % minimalPack > 0) 1 else 0
      val numberOfMinimalPacks = totalItems / minimalPack + zeroOrOne
      resultMap.+(minimalPack -> numberOfMinimalPacks)
    } else {
      val minimalPack = varietyOfPacksDescSorted.last
      val currentPack = varietyOfPacksDescSorted(currentPackIndex)
      val numberOfCurrentPacks = totalItems / currentPack
      val numberOfCurrentRestItems = totalItems % currentPack
      val nextPack = varietyOfPacksDescSorted(currentPackIndex + 1)
      val numberOfDifference = currentPack - numberOfCurrentRestItems
      val possibleDifference = nextPack - (totalItems % nextPack)
      if (totalItems.equals(currentPack)) {
        recursiveMinPacksPart(varietyOfPacksDescSorted, numberOfCurrentRestItems, currentPackIndex + 1, resultMap.+(currentPack -> 1))
      } else if (totalItems > currentPack) {
        recursiveMinPacksPart(varietyOfPacksDescSorted, numberOfCurrentRestItems, currentPackIndex + 1, resultMap.+(currentPack -> numberOfCurrentPacks))
      } else if (totalItems < currentPack) {
        if (possibleDifference == numberOfDifference) {
          if (possibleDifference / minimalPack >= 1) {
            recursiveMinPacksPart(varietyOfPacksDescSorted, totalItems, currentPackIndex + 1, resultMap.+(currentPack -> 0))
          } else {
            recursiveMinPacksPart(varietyOfPacksDescSorted, 0, currentPackIndex + 1, resultMap.+(currentPack -> 1))
          }
        } else {
          recursiveMinPacksPart(varietyOfPacksDescSorted, numberOfCurrentRestItems, currentPackIndex + 1, resultMap.+(currentPack -> 0))
        }
      } else {
        resultMap
      }
    }
  }

  def minPacks(varietyOfPacks: Seq[Int], totalItems: Int): Map[Int, Int] = {
    val varietyOfPacksDescSorted = varietyOfPacks.sortWith(_ > _)
    recursiveMinPacksPart(varietyOfPacksDescSorted, totalItems)
  }

  "min pack function test 1" should "be successful" in {
    val varietyOfPacks = Seq(5000, 2000, 1000, 500, 250)
    val totalItems = 12001
    val resultMap = minPacks(varietyOfPacks, totalItems)
    println(resultMap)
    assert(resultMap.equals(Map(5000 -> 2, 2000 -> 1, 1000 -> 0, 500 -> 0, 250 -> 1)))
  }

  "min pack function test 2" should "be successful" in {
    val varietyOfPacks = Seq(5000, 2000, 1000, 500, 250)
    val totalItems = 501
    val resultMap = minPacks(varietyOfPacks, totalItems)
    println(resultMap)
    assert(resultMap.equals(Map(5000 -> 0, 2000 -> 0, 1000 -> 0, 500 -> 1, 250 -> 1)))
  }

  "min pack function test 3" should "be successful" in {
    val varietyOfPacks = Seq(5000, 2000, 1000, 500, 250)
    val totalItems = 251
    val resultMap = minPacks(varietyOfPacks, totalItems)
    println(resultMap)
    assert(resultMap.equals(Map(5000 -> 0, 2000 -> 0, 1000 -> 0, 500 -> 1, 250 -> 0)))
  }

  "min pack function test 4" should "be successful" in {
    val varietyOfPacks = Seq(5000, 2000, 1000, 500, 250)
    val totalItems = 250
    val resultMap = minPacks(varietyOfPacks, totalItems)
    println(resultMap)
    assert(resultMap.equals(Map(5000 -> 0, 2000 -> 0, 1000 -> 0, 500 -> 0, 250 -> 1)))
  }

  "min pack function test 5" should "be successful" in {
    val varietyOfPacks = Seq(5000, 2000, 1000, 500, 250)
    val totalItems = 1
    val resultMap = minPacks(varietyOfPacks, totalItems)
    println(resultMap)
    assert(resultMap.equals(Map(5000 -> 0, 2000 -> 0, 1000 -> 0, 500 -> 0, 250 -> 1)))
  }

  // error (but does not correspond to common sense)
  "min pack function test 6" should "be successful" in {
    val varietyOfPacks = Seq(23, 31, 53)
    val totalItems = 263
    val resultMap = minPacks(varietyOfPacks, totalItems)
    println(resultMap)
  }

  // -------------- ANOTHER WAY -----------------

  def multiplesFunction(varietyOfPacks: Seq[Int], totalItems: Int): Option[Seq[Int]] = {
    val varietyOfPacksDescSorted = varietyOfPacks.sortWith(_ > _)
    val packsWithMultiples = varietyOfPacksDescSorted.map(number => number -> totalItems / number)
    // println(packsWithMultiples)
    val multiples = packsWithMultiples.map(_._2)
    // println(multiples)
    val mapWithNumbersMultipliedByCoefficients: Seq[(Int, Seq[(Int, Int)])] = packsWithMultiples.map(nC => {
      val SeqOfMultiplied = Seq.range(0, nC._2, 1).map(coefficient => coefficient -> nC._1 * coefficient)
      nC._1 -> SeqOfMultiplied
    })
    // mapWithNumbersMultipliedByCoefficients.foreach(seq => println(seq))

    val resMap = mutable.Map[Int, Seq[Int]]()

    multiples.size match {
      case 0 =>
        Array[Int]()
      case 1 =>
        Array.ofDim[Int](multiples.head)
      case 2 =>
        val a = Array.ofDim[Int](multiples.head, multiples(1))
        for {
          i <- 0 until multiples.head
          j <- 0 until multiples(1)
        } {
          a(i)(j) = mapWithNumbersMultipliedByCoefficients.head._2(i)._2 + mapWithNumbersMultipliedByCoefficients(1)._2(j)._2
          // println(s"($i)($j) = ${a(i)(j)}")
          resMap.+=(a(i)(j) -> Seq(i, j))
        }
      case 3 =>
        val a = Array.ofDim[Int](multiples.head, multiples(1), multiples(2))
        for {
          i <- 0 until multiples.head
          j <- 0 until multiples(1)
          k <- 0 until multiples(2)
        } {
          a(i)(j)(k) = mapWithNumbersMultipliedByCoefficients.head._2(i)._2 + mapWithNumbersMultipliedByCoefficients(1)._2(j)._2 + mapWithNumbersMultipliedByCoefficients(2)._2(k)._2
          // println(s"($i)($j)($k) = ${a(i)(j)(k)}")
          resMap.+=(a(i)(j)(k) -> Seq(i, j, k))
        }
      case 4 =>
        val a = Array.ofDim[Int](multiples.head, multiples(1), multiples(2), multiples(3))
        for {
          i <- 0 until multiples.head
          j <- 0 until multiples(1)
          k <- 0 until multiples(2)
          l <- 0 until multiples(3)
        } {
          a(i)(j)(k)(l) = mapWithNumbersMultipliedByCoefficients.head._2(i)._2 + mapWithNumbersMultipliedByCoefficients(1)._2(j)._2 + mapWithNumbersMultipliedByCoefficients(2)._2(k)._2 + mapWithNumbersMultipliedByCoefficients(3)._2(l)._2
          // println(s"($i)($j)($k)($l) = ${a(i)(j)(k)(l)}")
          resMap.+=(a(i)(j)(k)(l) -> Seq(i, j, k, l))
        }
      case 5 =>
        val a = Array.ofDim[Int](multiples.head, multiples(1), multiples(2), multiples(3), multiples(4))
        for {
          i <- 0 until multiples.head
          j <- 0 until multiples(1)
          k <- 0 until multiples(2)
          l <- 0 until multiples(3)
          m <- 0 until multiples(4)
        } {
          a(i)(j)(k)(l)(m) = mapWithNumbersMultipliedByCoefficients.head._2(i)._2 + mapWithNumbersMultipliedByCoefficients(1)._2(j)._2 + mapWithNumbersMultipliedByCoefficients(2)._2(k)._2 + mapWithNumbersMultipliedByCoefficients(3)._2(l)._2 + mapWithNumbersMultipliedByCoefficients(4)._2(m)._2
          // println(s"($i)($j)($k)($l)($m) = ${a(i)(j)(k)(l)(m)}")
          resMap.+=(a(i)(j)(k)(l)(m) -> Seq(i, j, k, l, m))
        }
    }

    println(s"attempt to find precision coefficients : ${resMap.get(totalItems)}")

    resMap.get(totalItems)

  }

  def minPacksMultiplesWay(varietyOfPacks: Seq[Int], totalItems: Int): Option[Any] = {
    val varietyOfPacksDescSorted = varietyOfPacks.sortWith(_ > _)
    multiplesFunction(varietyOfPacksDescSorted, totalItems)
  }

  "multiples pack function test" should "" in {
    val varietyOfPacks = Seq(23, 31, 53)
    val totalItems = 263
    val resultOption = minPacksMultiplesWay(varietyOfPacks, totalItems)
    println(resultOption)
  }

  def solution(varietyOfPacks: Seq[Int], totalItems: Int): Map[Int, Int] = {
    val varietyOfPacksDescSorted = varietyOfPacks.sortWith(_ > _)
    val opt = multiplesFunction(varietyOfPacksDescSorted, totalItems)
    opt match {
      case Some(coefficients) =>
        coefficients.zip(varietyOfPacksDescSorted).toMap
      case None =>
        minPacks(varietyOfPacks, totalItems)
    }
  }

  "solution test 1" should "be successful" in {
    val varietyOfPacks = Seq(5000, 2000, 1000, 500, 250)
    val totalItems = 12001
    val resultMap = solution(varietyOfPacks, totalItems)
    println(resultMap)
    assert(resultMap.equals(Map(5000 -> 2, 2000 -> 1, 1000 -> 0, 500 -> 0, 250 -> 1)))
  }

  "solution test 2" should "be successful" in {
    val varietyOfPacks = Seq(5000, 2000, 1000, 500, 250)
    val totalItems = 501
    val resultMap = solution(varietyOfPacks, totalItems)
    println(resultMap)
    assert(resultMap.equals(Map(5000 -> 0, 2000 -> 0, 1000 -> 0, 500 -> 1, 250 -> 1)))
  }

  "solution test 3" should "be successful" in {
    val varietyOfPacks = Seq(5000, 2000, 1000, 500, 250)
    val totalItems = 251
    val resultMap = solution(varietyOfPacks, totalItems)
    println(resultMap)
    assert(resultMap.equals(Map(5000 -> 0, 2000 -> 0, 1000 -> 0, 500 -> 1, 250 -> 0)))
  }

  "solution test 4" should "be successful" in {
    val varietyOfPacks = Seq(5000, 2000, 1000, 500, 250)
    val totalItems = 250
    val resultMap = solution(varietyOfPacks, totalItems)
    println(resultMap)
    assert(resultMap.equals(Map(5000 -> 0, 2000 -> 0, 1000 -> 0, 500 -> 0, 250 -> 1)))
  }

  "solution test 5" should "be successful" in {
    val varietyOfPacks = Seq(5000, 2000, 1000, 500, 250)
    val totalItems = 1
    val resultMap = solution(varietyOfPacks, totalItems)
    println(resultMap)
    assert(resultMap.equals(Map(5000 -> 0, 2000 -> 0, 1000 -> 0, 500 -> 0, 250 -> 1)))
  }

  def oddPairwiseProduct(a: List[Int], b: List[Int]): List[Int] = {
    val aFiltered = a.filter(e => e % 2 != 0)
    val bFiltered = b.filter(e => e % 2 != 0)
    val abFilteredSeq = Seq(aFiltered, bFiltered).sortBy(l => l.size)
    val minSize = Seq(aFiltered.size, bFiltered.size).min
    List(1, 2)
  }

}