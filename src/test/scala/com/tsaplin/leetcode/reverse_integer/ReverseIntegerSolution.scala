package com.tsaplin.leetcode.reverse_integer

import org.scalatest.FlatSpec

import scala.annotation.tailrec
import scala.util.Try

class ReverseIntegerSolution extends FlatSpec {

  object Solution {
    def reverse(x: Int): Int = {
      val stringNumber = x.toString
      if (x.equals(0)) {
        return 0
      }
      val longRes = if (stringNumber.head == '-') {
        ("-" + reversePositiveStringNumber(stringNumber.substring(1, stringNumber.length))).toLong
      } else {
        reversePositiveStringNumber(stringNumber).toLong
      }
      if (longRes > 2147483647) {
        0
      } else if (longRes < -2147483648) {
        0
      } else {
        longRes.toInt
      }
    }
    @tailrec
    def reversePositiveStringNumber(strNum: String): String = {
      if (strNum.head.equals('0')) {
        reversePositiveStringNumber(strNum.substring(1, strNum.length))
      } else {
        strNum.reverse
      }
    }
  }

  object OneLineSolution {
    def reverse(x: Int): Int = {
      val signMultiply = if (x < 0) -1 else 1
      Try(signMultiply * x.abs.toString.reverse.toInt).getOrElse(0)
    }
  }

  "sandbox" should "" in {
    println("00123".toInt)
  }

  "test 0" should "be successful" in {
    val number = 123
    val res = Solution.reverse(number)
    println(res)
    assert(res.equals(321))
  }

  "test 1" should "be successful" in {
    val number = 321
    val res = Solution.reverse(number)
    println(res)
    assert(res.equals(123))
  }

  "test 2" should "be successful" in {
    val number = -123
    val res = Solution.reverse(number)
    println(res)
    assert(res.equals(-321))
  }

  "test 3" should "be successful" in {
    val number = 120
    val res = Solution.reverse(number)
    println(res)
    assert(res.equals(21))
  }

  "test 4" should "be successful" in {
    val number = -120
    val res = Solution.reverse(number)
    println(res)
    assert(res.equals(-21))
  }


  "test 5" should "be successful" in {
    val number = 0
    val res = Solution.reverse(number)
    println(res)
    assert(res.equals(0))
  }

  "test 6" should "be successful" in {
    val number = 1534236469
    val res = Solution.reverse(number)
    println(res)
    assert(res.equals(0))
  }

  "test 7" should "be successful" in {
    val number = -2147483648
    val res = Solution.reverse(number)
    println(res)
    assert(res.equals(0))
  }

  "test 00" should "be successful" in {
    val number = 123
    val res = OneLineSolution.reverse(number)
    println(res)
    assert(res.equals(321))
  }

  "test 01" should "be successful" in {
    val number = 321
    val res = OneLineSolution.reverse(number)
    println(res)
    assert(res.equals(123))
  }

  "test 02" should "be successful" in {
    val number = -123
    val res = OneLineSolution.reverse(number)
    println(res)
    assert(res.equals(-321))
  }

  "test 03" should "be successful" in {
    val number = 120
    val res = OneLineSolution.reverse(number)
    println(res)
    assert(res.equals(21))
  }

  "test 04" should "be successful" in {
    val number = -120
    val res = OneLineSolution.reverse(number)
    println(res)
    assert(res.equals(-21))
  }


  "test 05" should "be successful" in {
    val number = 0
    val res = OneLineSolution.reverse(number)
    println(res)
    assert(res.equals(0))
  }

  "test 06" should "be successful" in {
    val number = 1534236469
    val res = OneLineSolution.reverse(number)
    println(res)
    assert(res.equals(0))
  }

  "test 07" should "be successful" in {
    val number = -2147483648
    val res = OneLineSolution.reverse(number)
    println(res)
    assert(res.equals(0))
  }

}
