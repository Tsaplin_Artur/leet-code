package com.tsaplin.leetcode.valid_parentheses

import org.scalatest.FlatSpec

import scala.annotation.tailrec

class ValidParentheses extends FlatSpec {

  object Solution {
    @tailrec
    def isValid(s: String): Boolean = {
      val s1 = s.replace("()", "")
      val s2 = s1.replace("{}", "")
      val s3 = s2.replace("[]", "")
      if (s3.nonEmpty && (s3.contains("()") || s3.contains("{}") || s3.contains("[]"))) {
        isValid(s3)
      } else if (s3.isEmpty) {
        true
      } else {
        false
      }
    }
  }

  "sandbox with string operations testing" should "demonstrate regular working with strings" in {

  }

  "test 0" should "be successful" in {
    val inputStr = "()"
    val res = Solution.isValid(inputStr)
    println(res)
    assert(res)
  }

  "test 1" should "be successful" in {
    val inputStr = "()[]{}"
    val res = Solution.isValid(inputStr)
    println(res)
    assert(res)
  }

  "test 2" should "be successful" in {
    val inputStr = "(]"
    val res = Solution.isValid(inputStr)
    println(res)
    assert(!res)
  }

  "test 3" should "be successful" in {
    val inputStr = "([])"
    val res = Solution.isValid(inputStr)
    println(res)
    assert(res)
  }

}
