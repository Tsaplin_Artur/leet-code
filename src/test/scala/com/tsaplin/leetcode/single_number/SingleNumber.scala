package com.tsaplin.leetcode.single_number

import org.scalatest.FlatSpec

class SingleNumber extends FlatSpec {

  // https://leetcode.com/problems/single-number/
  //
  // Given a non-empty array of integers, every element appears twice except for one. Find that single one.
  //
  // Note:
  //
  // Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory?
  //
  // Example 1:
  // Input: [2,2,1]
  // Output: 1
  //
  // Example 2:
  // Input: [4,1,2,1,2]
  // Output: 4

  object Solution {
    @scala.annotation.tailrec
    def singleNumber(nums: Array[Int]): Int = {
      if (nums.length == 0) {
        return -1
      }
      val firstElement = nums(0)
      val filteredNums = nums.filter(elem => elem != firstElement)
      if (nums.length - filteredNums.length > 1) {
        singleNumber(filteredNums)
      } else {
        firstElement
      }
    }
    // Runtime: 492 ms, faster than 100.00% of Scala online submissions for Single Number.
    // Memory Usage: 52.4 MB, less than 75.32% of Scala online submissions for Single Number.
    def inlineSolution(nums: Array[Int]): Int = {
      nums.reduce((a, b) => a ^ b) // bitwise XOR gives in result only non paired number in array
    }
  }

  "test 0" should "be successful" in {
    val testArrayOne: Array[Int] = Array(4,1,2,1,2)
    assert(Solution.inlineSolution(testArrayOne) == 4)
  }

  "test 1" should "be successful" in {
    val testArrayOne: Array[Int] = Array(1,2,1,2,3,3,4,5,5)
    assert(Solution.inlineSolution(testArrayOne) == 4)
  }

}
