package com.tsaplin.leetcode.two_sum

import org.scalatest.FlatSpec

import scala.collection.mutable

class TwoSum extends FlatSpec {

  // https://leetcode.com/problems/two-sum/
  //
  // Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
  //
  // You may assume that each input would have exactly one solution, and you may not use the same element twice.
  //
  // You can return the answer in any order.
  //
  // Example 1:
  // Input: nums = [2,7,11,15], target = 9
  // Output: [0,1]
  // Output: Because nums[0] + nums[1] == 9, we return [0, 1].
  //
  // Example 2:
  // Input: nums = [3,2,4], target = 6
  // Output: [1,2]
  //
  // Example 3:
  // Input: nums = [3,3], target = 6
  // Output: [0,1]

  object Solution {
    def twoSum(nums: Array[Int], target: Int): Array[Int] = {
      val map = mutable.Map.empty[Int, Int]
      val str: String = "string"
      for (i <- nums.indices) {
        val currentValue = nums(i)
        val result = map.get(target - currentValue)
        if (result.isDefined) return Array(result.get, i)
        map.put(currentValue, i)
      }
      Array(-1, -1)
    }
  }

  "test 0" should "be successful" in {
    val testArrayOne: Array[Int] = Array(3, 2, 4)
    val target = 6
    val result = Solution.twoSum(testArrayOne, target)
    println(result.mkString(", "))
    assert(result sameElements Array(1, 2))
  }

}
