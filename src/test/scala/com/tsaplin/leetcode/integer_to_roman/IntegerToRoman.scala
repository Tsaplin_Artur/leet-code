package com.tsaplin.leetcode.integer_to_roman

import org.scalatest.FlatSpec

//Symbol	Value
//I	1
//V	5
//X	10
//L	50
//C	100
//D	500
//M	1000
//Roman numerals are formed by appending the conversions of decimal place values from highest to lowest. Converting a decimal place value into a Roman numeral has the following rules:
//
//If the value does not start with 4 or 9, select the symbol of the maximal value that can be subtracted from the input, append that symbol to the result, subtract its value, and convert the remainder to a Roman numeral.
//If the value starts with 4 or 9 use the subtractive form representing one symbol subtracted from the following symbol, for example, 4 is 1 (I) less than 5 (V): IV and 9 is 1 (I) less than 10 (X): IX. Only the following subtractive forms are used: 4 (IV), 9 (IX), 40 (XL), 90 (XC), 400 (CD) and 900 (CM).
//Only powers of 10 (I, X, C, M) can be appended consecutively at most 3 times to represent multiples of 10. You cannot append 5 (V), 50 (L), or 500 (D) multiple times. If you need to append a symbol 4 times use the subtractive form.
//Given an integer, convert it to a Roman numeral.
//
//
//
//Example 1:
//
//Input: num = 3749
//
//Output: "MMMDCCXLIX"
//
//Explanation:
//
//3000 = MMM as 1000 (M) + 1000 (M) + 1000 (M)
// 700 = DCC as 500 (D) + 100 (C) + 100 (C)
//  40 = XL as 10 (X) less of 50 (L)
//   9 = IX as 1 (I) less of 10 (X)
//Note: 49 is not 1 (I) less of 50 (L) because the conversion is based on decimal places
//Example 2:
//
//Input: num = 58
//
//Output: "LVIII"
//
//Explanation:
//
//50 = L
// 8 = VIII
//Example 3:
//
//Input: num = 1994
//
//Output: "MCMXCIV"
//
//Explanation:
//
//1000 = M
// 900 = CM
//  90 = XC
//   4 = IV
//
//
//Constraints:
//
//1 <= num <= 3999

class IntegerToRoman extends FlatSpec {

  object Solution {
    def intToRoman(num: Int): String = {
      if (num > 3999 || num < 1) {
        ""
      } else {
        val thousands = num / 1000
        val hundreds = (num % 1000) / 100
        val tens = (num % 1000) % 100 / 10
        val units = (num % 1000) % 100 % 10
        s"${thousandsToString(thousands)}${hundredsToString(hundreds)}${tensToString(tens)}${unitsToString(units)}"
      }
    }
    def thousandsToString(t: Int): String = {
      t match {
        case 0 => ""
        case 1 => "M"
        case 2 => "MM"
        case 3 => "MMM"
        case _ => "1000?"
      }
    }
    def hundredsToString(h: Int): String = {
      h match {
        case 0 => ""
        case 1 => "C"
        case 2 => "CC"
        case 3 => "CCC"
        case 4 => "CD"
        case 5 => "D"
        case 6 => "DC"
        case 7 => "DCC"
        case 8 => "DCCC"
        case 9 => "CM"
        case _ => "100?"
      }
    }
    def tensToString(t: Int): String = {
      t match {
        case 0 => ""
        case 1 => "X"
        case 2 => "XX"
        case 3 => "XXX"
        case 4 => "XL"
        case 5 => "L"
        case 6 => "LX"
        case 7 => "LXX"
        case 8 => "LXXX"
        case 9 => "XC"
        case _ => "10?"
      }
    }
    def unitsToString(u: Int): String = {
      u match {
        case 0 => ""
        case 1 => "I"
        case 2 => "II"
        case 3 => "III"
        case 4 => "IV"
        case 5 => "V"
        case 6 => "VI"
        case 7 => "VII"
        case 8 => "VIII"
        case 9 => "IX"
        case _ => "1?"
      }
    }
  }

  "test 0" should "be successful" in {
    val romanResult = Solution.intToRoman(3749)
    println(romanResult)
    assert(romanResult.equals("MMMDCCXLIX"))
  }


  "test 1" should "be successful" in {
    val romanResult = Solution.intToRoman(58)
    println(romanResult)
    assert(romanResult.equals("LVIII"))
  }

  "test 2" should "be successful" in {
    val romanResult = Solution.intToRoman(1994)
    println(romanResult)
    assert(romanResult.equals("MCMXCIV"))
  }

}
