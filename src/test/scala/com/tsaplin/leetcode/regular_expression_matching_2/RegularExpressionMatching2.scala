package com.tsaplin.leetcode.regular_expression_matching_2

import org.scalatest.FlatSpec

import scala.language.implicitConversions

// https://leetcode.com/problems/regular-expression-matching/
class RegularExpressionMatching2 extends FlatSpec {

  //  Given an input string s and a pattern p, implement regular expression matching with support for '.' and '*' where:
  //
  //  '.' Matches any single character.
  //  '*' Matches zero or more of the preceding element.
  //  The matching should cover the entire input string (not partial).
  //
  //
  //
  //  Example 1:
  //
  //  Input: s = "aa", p = "a"
  //  Output: false
  //  Explanation: "a" does not match the entire string "aa".
  //
  //  Example 2:
  //
  //  Input: s = "aa", p = "a*"
  //  Output: true
  //  Explanation: '*' means zero or more of the preceding element, 'a'. Therefore, by repeating 'a' once, it becomes "aa".
  //
  //  Example 3:
  //
  //  Input: s = "ab", p = ".*"
  //  Output: true
  //  Explanation: ".*" means "zero or more (*) of any character (.)".
  //
  //
  //  Constraints:
  //
  //  1 <= s.length <= 20
  //  1 <= p.length <= 20
  //  s contains only lowercase English letters.
  //  p contains only lowercase English letters, '.', and '*'.
  //  It is guaranteed for each appearance of the character '*', there will be a previous valid character to match.

  object Solution {

    def isMatchByChatGPT(str: String, pattern: String): Boolean = {
      val strLength = str.length
      val patternLength = pattern.length

      // DP таблица для хранения результатов
      val dp = Array.ofDim[Boolean](strLength + 1, patternLength + 1)

      // Инициализация: пустая строка и пустой паттерн совпадают
      dp(0)(0) = true

      // Паттерн типа "a*" или ".*" может совпасть с пустой строкой
      for (patternIterator <- 2 to patternLength) {
        if (pattern(patternIterator - 1) == '*') {
          dp(0)(patternIterator) = dp(0)(patternIterator - 2)
        }
      }

      // Заполнение таблицы DP
      for (strIterator <- 1 to strLength) {
        for (patternIterator <- 1 to patternLength) {
          if (pattern(patternIterator - 1) == str(strIterator - 1) || pattern(patternIterator - 1) == '.') {
            dp(strIterator)(patternIterator) = dp(strIterator - 1)(patternIterator - 1)
          } else if (pattern(patternIterator - 1) == '*') {
            // Смотрим два случая для *
            // 1. Игнорируем символ перед '*'
            dp(strIterator)(patternIterator) = dp(strIterator)(patternIterator - 2)
            // 2. Символ перед '*' совпадает с текущим символом строки
            if (pattern(patternIterator - 2) == str(strIterator - 1) || pattern(patternIterator - 2) == '.') {
              dp(strIterator)(patternIterator) = dp(strIterator)(patternIterator) || dp(strIterator - 1)(patternIterator)
            }
          }
        }
      }

      dp(strLength)(patternLength)
    }

    def removeFoulCombinations(arr: Array[String]): Array[String] = {
      // using fold for array iteration and making result array
      arr.foldLeft(List[String]()) { (accumulator, current) =>
        if (accumulator.isEmpty) accumulator :+ current
        else if (accumulator.nonEmpty && accumulator.last.contains('*') && accumulator.last.equals(current)) accumulator
        else accumulator :+ current
      }.toArray
    }

    def patternSimplePartsMaker(pattern: String): Array[String] = {
      val modifiedPattern = pattern
        .replace(".*", ",.*,")
        .replace(".", ",.,")
        .replace(".,*", ".*")
        .replace("*", "*,")
      modifiedPattern
        .split(",")
        .filter(_.nonEmpty)
        .map { str =>
          if (str.endsWith("*") && str.length > 2) {
            str.dropRight(2) + ";" + str.takeRight(2)
          } else {
            str
          }
        }
        .mkString(";")
        .split(";")
    }

    def isMatch(s: String, p: String): Boolean = {
      val simplePatternArray = patternSimplePartsMaker(p)
      recursivePatternMatchingChecking(s, simplePatternArray)
    }

    def recursivePatternMatchingChecking(stringForCheck: String, simplePatternsArray: Array[String]): Boolean = {
      if (stringForCheck.isEmpty && simplePatternsArray.length == 0) { // case with empty pattern and empty string
        true
      } else if (stringForCheck.nonEmpty && simplePatternsArray.length > 0) {
        (simplePatternsArray.head.charAt(0), simplePatternsArray.head.last) match {
          case ('.', '.') =>
            // one any element
            recursivePatternMatchingChecking(stringForCheck.substring(1, stringForCheck.length), simplePatternsArray.slice(1, simplePatternsArray.length))
          case ('.', '*') =>
            // many any elements
            val recCall = recursivePatternMatchingChecking(stringForCheck, simplePatternsArray.slice(1, simplePatternsArray.length))
            if (recCall) {
              true
            } else {
              if (simplePatternsArray.length > 1) {
                // case when we have some elements after many any elements...
                val newString = stringForCheck.substring(1, stringForCheck.length)
                if (stringForCheck.head == simplePatternsArray(1).head) {
                  // case when rest string matching for next simple pattern
                  recursivePatternMatchingChecking(newString, simplePatternsArray)
                } else {
                  // case when we have no matching for next pattern
                  recursivePatternMatchingChecking(newString, simplePatternsArray)
                }
              } else {
                // case when we have last pattern with many any elements
                val newString = stringForCheck.substring(1, stringForCheck.length)
                recursivePatternMatchingChecking(newString, simplePatternsArray)
              }
            }
          case (_, '*') =>
            // case when zero elements of this pattern
            val recCall = recursivePatternMatchingChecking(stringForCheck, simplePatternsArray.slice(1, simplePatternsArray.length))
            if (recCall) {
              true
            } else {
              if (simplePatternsArray.length > 1 && !simplePatternsArray(1).head.equals(simplePatternsArray.head.head)) {
                if (simplePatternsArray.head.charAt(0) == stringForCheck.head && simplePatternsArray(1).charAt(0) != stringForCheck.head) {
                  // case when we have matching current string element for pattern
                  val newString = stringForCheck.substring(1, stringForCheck.length)
                  recursivePatternMatchingChecking(newString, simplePatternsArray)
                } else {
                  false
                }
              } else if (simplePatternsArray.length > 1 && simplePatternsArray(1).head.equals(simplePatternsArray.head.head)) {
                // case when pattern have a*a sequence
                if (!simplePatternsArray(1).head.equals(stringForCheck.head)) {
                  false
                } else {
                  val newString = stringForCheck.substring(1, stringForCheck.length)
                  recursivePatternMatchingChecking(newString, simplePatternsArray)
                }
              } else {
                // case when we have last pattern with many any elements
                if (stringForCheck.head.equals(simplePatternsArray.head.head)) {
                  // case like this - pattern = "d*", string = "aabd"
                  val newString = stringForCheck.substring(1, stringForCheck.length)
                  recursivePatternMatchingChecking(newString, simplePatternsArray)
                } else {
                  false
                }
              }
            }
          case (_, _) =>
            if (simplePatternsArray.length == 1 && simplePatternsArray.head.equals(stringForCheck)) {
              true
            } else if (stringForCheck.length < simplePatternsArray.head.length) {
              false
            } else {
              val stringForMatching = stringForCheck.substring(0, simplePatternsArray.head.length)
              // pattern matching for certain combinations of certain elements
              if (simplePatternsArray.head.length > 1 && simplePatternsArray.head.length <= stringForCheck.length && simplePatternsArray.head.equals(stringForMatching)) {
                val newString = stringForCheck.substring(simplePatternsArray.head.length, stringForCheck.length)
                val newPattern = simplePatternsArray.slice(1, simplePatternsArray.length)
                recursivePatternMatchingChecking(newString, newPattern)
              } else if (simplePatternsArray.head.length > 1 && simplePatternsArray.head.length <= stringForCheck.length && !simplePatternsArray.head.equals(stringForMatching)) {
                false
              } else if (simplePatternsArray.head.length == 1 && simplePatternsArray.head.length <= stringForCheck.length && !simplePatternsArray.head.equals(stringForMatching)) {
                false
              } else if (simplePatternsArray.head.length == 1 && simplePatternsArray.head.length <= stringForCheck.length && simplePatternsArray.head.equals(stringForMatching)) {
                val newString = stringForCheck.substring(simplePatternsArray.head.length, stringForCheck.length)
                val newPattern = simplePatternsArray.slice(1, simplePatternsArray.length)
                recursivePatternMatchingChecking(newString, newPattern)
              } else {
                false
              }
            }
        }
      } else if (stringForCheck.isEmpty && simplePatternsArray.length > 0) {
        if (simplePatternsArray.forall(str => str.contains('*'))) {
          // case when we have only * patterns, which allowed zero elemnts in string
          true
        } else {
          // case when pattern have one certain element and rest string is empty
          false
        }
      } else {
        // some strange another cases
        false
      }
    }

  }

  "test 0" should "working well" in {
    val testPattern = "aa"
    val testString = "aa"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatch(testString, testPattern)
    assert(isMatch)
  }

  "test 1" should "working well" in {
    val testPattern = "aa"
    val testString = "ab"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatch(testString, testPattern)
    assert(!isMatch)
  }

  "test 2" should "working well" in {
    val testPattern = "aa"
    val testString = "a"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatch(testString, testPattern)
    assert(!isMatch)
  }

  "test 3" should "working well" in {
    val testPattern = "a"
    val testString = "aa"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatch(testString, testPattern)
    assert(!isMatch)
  }

  "test 4" should "working well" in {
    val testPattern = "a*"
    val testString = "aa"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatch(testString, testPattern)
    assert(isMatch)
  }

  "test 5" should "working well" in {
    val testPattern = ".*"
    val testString = "ab"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatch(testString, testPattern)
    assert(isMatch)
  }

  "test 6" should "working well" in {
    val testPattern = "mis*is*p*."
    val testString = "mississippi"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatch(testString, testPattern)
    assert(!isMatch)
  }

  "test 7" should "working well" in {
    val testPattern = "c*a*b"
    val testString = "aab"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatch(testString, testPattern)
    assert(isMatch)
  }

  "test 8" should "working well" in {
    val testPattern = "d*"
    val testString = "abcd"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatch(testString, testPattern)
    assert(!isMatch)
  }

  "test 9" should "working well" in {
    val testPattern = "a.cd*efgh*.*"
    val testString = "amcddddefghhhhabcde"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatch(testString, testPattern)
    assert(isMatch)
  }

  "test 10" should "working well" in {
    val testPattern = ".a"
    val testString = "aaa"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatch(testString, testPattern)
    assert(!isMatch)
  }

  "test 11" should "working well" in {
    val testPattern = "a*a"
    val testString = "aaa"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatch(testString, testPattern)
    assert(isMatch)
  }

  "test 12" should "working well" in {
    val testPattern = "ab*a*c*a"
    val testString = "aaa"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatch(testString, testPattern)
    assert(isMatch)
  }

  "test 13" should "working well" in {
    val testPattern = ".*..a*"
    val testString = "a"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatch(testString, testPattern)
    assert(!isMatch)
  }

  "test 14" should "working well" in {
    val testPattern = ".*..c*"
    val testString = "ab"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatch(testString, testPattern)
    assert(isMatch)
  }

  "test 15" should "working well" in {
    val testPattern = "aaaa"
    val testString = "bab"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatch(testString, testPattern)
    assert(!isMatch)
  }

  "test 16" should "working well" in {
    val testPattern = "a*.*b*.*a*aa*a*"
    val testString = "acaabbaccbbacaabbbb"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatch(testString, testPattern)
    assert(!isMatch)
  }

  "test 17" should "working well" in {
    val testPattern = "ac*.a*ac*.*ab*b*ac"
    val testString = "acbbcbcbcbaaacaac"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatch(testString, testPattern)
    assert(!isMatch)
  }

  "test for non matching string with patterning after .*" should "answering false, but answering true" in {
    val testPattern = "a.cd*efgh*.*aaaa"
    val testString = "amcddddefghhhhabcde"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatch(testString, testPattern)
    assert(!isMatch)
  }

  "test for non matching string with patterning after .* 2" should "answering false, but answering true" in {
    val testPattern = ".*c"
    val testString = "ab"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatch(testString, testPattern)
    assert(!isMatch)
  }

  "remove foul combinations test" should "" in {
    val testPattern = "abcd*d*"
    println(Solution.removeFoulCombinations(Solution.patternSimplePartsMaker(testPattern)).mkString(";"))
  }

  "sandbox" should "" in {
    val str = "ab"
    println(str.substring(1, 2))
    val arr = Array[String]("a", "b", "c")
    println(arr.slice(1, arr.length).mkString(","))
  }

  // Chat GPT Solution
  "test 0 Chat GPT Solution" should "working well" in {
    val testPattern = "aa"
    val testString = "aa"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatchByChatGPT(testString, testPattern)
    assert(isMatch)
  }

  "test 1 Chat GPT Solution" should "working well" in {
    val testPattern = "aa"
    val testString = "ab"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatchByChatGPT(testString, testPattern)
    assert(!isMatch)
  }

  "test 2 Chat GPT Solution" should "working well" in {
    val testPattern = "aa"
    val testString = "a"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatchByChatGPT(testString, testPattern)
    assert(!isMatch)
  }

  "test 3 Chat GPT Solution" should "working well" in {
    val testPattern = "a"
    val testString = "aa"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatchByChatGPT(testString, testPattern)
    assert(!isMatch)
  }

  "test 4 Chat GPT Solution" should "working well" in {
    val testPattern = "a*"
    val testString = "aa"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatchByChatGPT(testString, testPattern)
    assert(isMatch)
  }

  "test 5 Chat GPT Solution" should "working well" in {
    val testPattern = ".*"
    val testString = "ab"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatchByChatGPT(testString, testPattern)
    assert(isMatch)
  }

  "test 6 Chat GPT Solution" should "working well" in {
    val testPattern = "mis*is*p*."
    val testString = "mississippi"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatchByChatGPT(testString, testPattern)
    assert(!isMatch)
  }

  "test 7 Chat GPT Solution" should "working well" in {
    val testPattern = "c*a*b"
    val testString = "aab"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatchByChatGPT(testString, testPattern)
    assert(isMatch)
  }

  "test 8 Chat GPT Solution" should "working well" in {
    val testPattern = "d*"
    val testString = "abcd"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatchByChatGPT(testString, testPattern)
    assert(!isMatch)
  }

  "test 9 Chat GPT Solution" should "working well" in {
    val testPattern = "a.cd*efgh*.*"
    val testString = "amcddddefghhhhabcde"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatchByChatGPT(testString, testPattern)
    assert(isMatch)
  }

  "test 10 Chat GPT Solution" should "working well" in {
    val testPattern = ".a"
    val testString = "aaa"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatchByChatGPT(testString, testPattern)
    assert(!isMatch)
  }

  "test 11 Chat GPT Solution" should "working well" in {
    val testPattern = "a*a"
    val testString = "aaa"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatchByChatGPT(testString, testPattern)
    assert(isMatch)
  }

  "test 12 Chat GPT Solution" should "working well" in {
    val testPattern = "ab*a*c*a"
    val testString = "aaa"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatchByChatGPT(testString, testPattern)
    assert(isMatch)
  }

  "test 13 Chat GPT Solution" should "working well" in {
    val testPattern = ".*..a*"
    val testString = "a"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatchByChatGPT(testString, testPattern)
    assert(!isMatch)
  }

  "test 14 Chat GPT Solution" should "working well" in {
    val testPattern = ".*..c*"
    val testString = "ab"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatchByChatGPT(testString, testPattern)
    assert(isMatch)
  }

  "test 15 Chat GPT Solution" should "working well" in {
    val testPattern = "aaaa"
    val testString = "bab"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatchByChatGPT(testString, testPattern)
    assert(!isMatch)
  }

  "test 16 Chat GPT Solution" should "working well" in {
    val testPattern = "a*.*b*.*a*aa*a*"
    val testString = "acaabbaccbbacaabbbb"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatchByChatGPT(testString, testPattern)
    assert(!isMatch)
  }

  "test 17 Chat GPT Solution" should "working well" in {
    val testPattern = "ac*.a*ac*.*ab*b*ac"
    val testString = "acbbcbcbcbaaacaac"
    println("simple patterns array: " + Solution.patternSimplePartsMaker(testPattern).mkString(", "))
    val isMatch = Solution.isMatchByChatGPT(testString, testPattern)
    assert(!isMatch)
  }

}