package com.tsaplin.leetcode.regular_expression_matching

import org.scalatest.FlatSpec

import scala.collection.immutable
import scala.util.matching.Regex

class RegularExpressionMatching extends FlatSpec {

  // Given an input string (s) and a pattern (p), implement regular expression matching with support for '.' and '*'.
  //
  // '.' Matches any single character.
  // '*' Matches zero or more of the preceding element.
  // The matching should cover the entire input string (not partial).
  //
  // Note:
  // s could be empty and contains only lowercase letters a-z.
  // p could be empty and contains only lowercase letters a-z, and characters like . or *.
  //
  // Example 1:
  // Input:
  // s = "aa"
  // p = "a"
  // Output: false
  // Explanation: "a" does not match the entire string "aa".
  //
  // Example 2:
  // Input:
  // s = "aa"
  // p = "a*"
  // Output: true
  // Explanation: '*' means zero or more of the preceding element, 'a'. Therefore, by repeating 'a' once, it becomes "aa".
  //
  // Example 3:
  // Input:
  // s = "ab"
  // p = ".*"
  // Output: true
  // Explanation: ".*" means "zero or more (*) of any character (.)".
  //
  // Example 4:
  // Input:
  // s = "aab"
  // p = "c*a*b"
  // Output: true
  // Explanation: c can be repeated 0 times, a can be repeated 1 time. Therefore, it matches "aab".
  //
  // Example 5:
  // Input:
  // s = "mississippi"
  // p = "mis*is*p*."
  // Output: false

  // Solution is - we need to separate string to block's: 'Aa-Zz (Any character)', '(Aa-Zz)*', '.*', '.' - after that check string for sequence of this block


  trait Sequence

  case class EmptyString() extends Sequence

  case class AnyCharSequence() extends Sequence

  case class CertainCharSequence() extends Sequence

  case class OneAnyChar() extends Sequence

  case class CertainDifferentCharSequence() extends Sequence

  case class StringForParsing() extends Sequence

  object Solution {

    def combine(first: List[(Sequence, String)], second: List[(Sequence, String)]): List[(Sequence, String)] = (first, second) match {
      case (Nil, Nil) => List()
      case (Nil, y :: Nil) => List(y)
      case (x :: Nil, Nil) => List(x)
      case (x :: xs, _ :: ys) if xs.size >= ys.size => x :: combine(xs, second)
      case (_ :: xs, y :: ys) if xs.size < ys.size => y :: combine(first, ys)
    }

    def isMatch(s: String, p: String): Boolean = {
      true
    }
  }

  "test 0" should "working well" in {
    // incoming string
    val str = "AAAabcdBBBbbbCCCd"
    val patternStr = "AAA.*BBBb*CCC"
    // patterns
    val anyNumberOfAnyCharSequencePattern = new Regex("[.][*]")   // priority - 1
    val anyNumberOfOneCharSequencePattern = new Regex("[A-z][*]") // priority - 2
    val anyOneCharPattern = new Regex("[.]")                      // priority - 3
    // logic
    // .....
    // first sorting
    val anyCharSequenceBlocks: List[(Sequence, String)] = anyNumberOfAnyCharSequencePattern.findAllIn(patternStr).toList.map(item => (AnyCharSequence(), item))
    val anyCharSequenceEdges: List[(Sequence, String)] = anyNumberOfAnyCharSequencePattern.split(patternStr).toList.map(item => (StringForParsing(), item))
    var sortedTemplate: immutable.Seq[(Any, String)] = Solution.combine(anyCharSequenceBlocks, anyCharSequenceEdges)
    sortedTemplate = sortedTemplate.filter(item => item != (StringForParsing(), ""))
    // second sorting
    val secondPrioritySortingTemplate = sortedTemplate.filter(item => item._1 == StringForParsing()).map(itm => {
      val oneCharSequenceBlocks: List[(Sequence, String)] = anyNumberOfOneCharSequencePattern.findAllIn(itm._2).toList.map(str => (CertainCharSequence(), str))
      val oneCharSequenceEdges: List[(Sequence, String)] = anyNumberOfOneCharSequencePattern.split(itm._2).toList.map(str => (StringForParsing(), str))
      val secondtPrioritySortedList = Solution.combine(oneCharSequenceBlocks, oneCharSequenceEdges)
      (itm, secondtPrioritySortedList)
    })
    secondPrioritySortingTemplate.foreach(item => {
      val indx = sortedTemplate.indexOf(item._1)
      sortedTemplate = sortedTemplate.patch(indx, item._2, 1)
    })
    sortedTemplate = sortedTemplate.filter(item => item != (StringForParsing(), ""))
    // third sorting
    val thirdPrioritySortingTemplate = sortedTemplate.filter(item => item._1 == StringForParsing()).map(itm => {
      val oneCharBlocks: List[(Sequence, String)] = anyOneCharPattern.findAllIn(itm._2).toList.map(str => (OneAnyChar(), str))
      val oneCharEdges: List[(Sequence, String)] = anyOneCharPattern.split(itm._2).toList.map(str => (StringForParsing(), str))
      val thirdPrioritySortedList = Solution.combine(oneCharBlocks, oneCharEdges)
      (itm, thirdPrioritySortedList)
    })
    thirdPrioritySortingTemplate.foreach(item => {
      val indx = sortedTemplate.indexOf(item._1)
      sortedTemplate = sortedTemplate.patch(indx, item._2, 1)
    })
    sortedTemplate = sortedTemplate.filter(item => item != (StringForParsing(), ""))
    // last sorting (replace string for parsing to certain different char sequence
    sortedTemplate = sortedTemplate.map(item => if (item._1 == StringForParsing()) { (CertainDifferentCharSequence(), item._2) } else item )
    System.out.println("AFTER TEMPLATING")
    sortedTemplate.foreach(item => {
      System.out.println(item)
    })


  }


  "test 1" should "working well" in {
    val letterSequencePattern = new Regex("[A-z]+")
    val str = "AAbbCCDd12345678"
    val sampling: Regex.MatchIterator = letterSequencePattern.findAllIn(str)
    sampling.foreach(item => {
      System.out.println(item)
      assert(item.equals("AAbbCCDd"))
    })
  }

  "test 2" should "working well" in {
    val letterPointSequencePattern = new Regex("[.][*]")
    val str = "AAbbCCDd12345Aa.*678"
    val sampling: Regex.MatchIterator = letterPointSequencePattern.findAllIn(str)
    sampling.foreach(item => {
      System.out.println(item)
      assert(item.equals(".*"))
    })
  }

  "test 3" should "working well" in {
    val letterPointSequencePattern = new Regex("[.][A-z]")
    val str = "AAbbCCDd12345Aa.*6.a78"
    val sampling: Regex.MatchIterator = letterPointSequencePattern.findAllIn(str)
    sampling.foreach(item => {
      System.out.println(item)
      assert(item.equals(".a"))
    })
  }

  "test 4" should "working well" in {
    val str = "678.Aasaadsfs"
    val anyOneCharPattern = new Regex("[.]")
    val oneCharBlocks: List[(Sequence, String)] = anyOneCharPattern.findAllIn(str).toList.map(str => (OneAnyChar(), str))
    val oneCharEdges: List[(Sequence, String)] = anyOneCharPattern.split(str).toList.map(str => (StringForParsing(), str))
    val sortedList = Solution.combine(oneCharBlocks, oneCharEdges)
    sortedList.foreach(itm => System.out.println(itm))
  }

  "test 5" should "working well" in {
    val str = "678.*Aasa.*adsfs.*"
    val anyOneCharPattern = new Regex("[.][*]")
    val oneCharBlocks: List[(Sequence, String)] = anyOneCharPattern.findAllIn(str).toList.map(str => (OneAnyChar(), str))
    val oneCharEdges: List[(Sequence, String)] = anyOneCharPattern.split(str).toList.map(str => (StringForParsing(), str))
    val sortedList = Solution.combine(oneCharEdges, oneCharBlocks)
    sortedList.foreach(itm => System.out.println(itm))
  }

  "test 6" should "working well" in {
    val str = ".*AAAA.*BBBB.*CCCC"
    val anyOneCharPattern = new Regex("[.][*]")
    val oneCharBlocks: List[(Sequence, String)] = anyOneCharPattern.findAllIn(str).toList.map(str => (OneAnyChar(), str))
    val oneCharEdges: List[(Sequence, String)] = anyOneCharPattern.split(str).toList.map(str => (StringForParsing(), str))
    val sortedList = Solution.combine(oneCharEdges, oneCharBlocks)
    sortedList.foreach(itm => System.out.println(itm))
  }

}
